package com.cocolove2.andbase.index;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.cocolove2.andbase.BaseActivity;
import com.cocolove2.andbase.R;
import com.cocolove2.andbase.http.HttpTestActivity;
import com.cocolover2.andbase.adapterdelegates.AbsAdapterDelegateBase;
import com.cocolover2.andbase.adapterdelegates.AbsSampleAdapterDelegate;
import com.cocolover2.andbase.adapterdelegates.BaseDelegationAdapter;
import com.cocolover2.andbase.adapterdelegates.RecyclerViewHolder;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {
    List<String> mDatas = new ArrayList<>();
    BaseDelegationAdapter<List<String>> mAdapter;

    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.main_index_rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mDatas.addAll(buildItemDatas());
        mAdapter = new BaseDelegationAdapter<List<String>>(mDatas);
        MyDelegate delegate = new MyDelegate(this);
        mAdapter.addDelegate(delegate);
        delegate.setOnItemClickListener(new AbsAdapterDelegateBase.OnItemClikListener() {
            @Override
            public void onItemClicked(int viewType, int position) {
                handleItemClick(position);
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }

    private void handleItemClick(int position) {
        switch (position) {
            case 0:
                startActivity(new Intent(this, HttpTestActivity.class));
                break;
            default:
                Toast.makeText(this, "敬请期待", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private List<String> buildItemDatas() {
        List<String> datas = new ArrayList<>();
        datas.add("HTTP网络请求");
        return datas;
    }

    private class MyDelegate extends AbsSampleAdapterDelegate<String, String> {

         MyDelegate(Context context) {
            super(context, R.layout.item_main_index, 1);
        }

        @Override
        public void onBindData(RecyclerViewHolder holder, int position, String item) {
            holder.setText(R.id.item_main_index_btn, item);
        }

        @Override
        public boolean isForViewType(@NonNull List<String> items, int position) {
            return true;
        }
    }

}

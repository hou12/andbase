package com.cocolove2.andbase;

import com.cocolover2.andbase.mvpbase.AndBasePresenter;
import com.cocolover2.andbase.mvpbase.IAndBaseMVPView;


@SuppressWarnings("unchecked")
public class BasePresenter<V extends IAndBaseMVPView> extends AndBasePresenter<V> {


    @Override
    public ApiHelper getApiHelper() {
        return ApiHelper.getInstance();
    }


//    /**
//     * 多文件上传
//     */
//    public void uploadFiles(Map<String, Object> params, final OnHttpListener<List<UploadFileResult>> listener) {
//        String url = ApiHelper.BASE_URL + "wd/business/business!uploadFile.json";
//        Subscription subscription = getApiHelper().uploadFiles(url, params, new
//                OnHttpListener<String>() {
//                    @Override
//                    public void onSuccess(String response) {
//                        try {
//                            JSONObject object = new JSONObject(response);
//                            String jsonArray = object.getString("data");
//                            if (listener != null) {
//                                List<UploadFileResult> list = new Gson().fromJson(jsonArray, new
//                                        TypeToken<List<UploadFileResult>>() {
//                                        }.getType());
//                                listener.onSuccess(list);
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            listener.onFailure(-1000, e.getMessage());
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(int code, String msg) {
//                        if (listener != null)
//                            listener.onFailure(code, msg);
//                    }
//                });
//
//        addSubscription(subscription);
//    }

}

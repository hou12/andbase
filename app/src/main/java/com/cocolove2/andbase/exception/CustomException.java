package com.cocolove2.andbase.exception;

/**
 * 业务异常
 */
public class CustomException extends Exception {
    private int code;
    private String msg;

    public CustomException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}

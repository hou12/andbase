package com.cocolove2.andbase.exception;

public class TokenEmptyOrInvalidException extends Exception {
    private int code;
    private String msg;

    public TokenEmptyOrInvalidException(int code, String msg) {
        super("token is empty or invalid,please refresh your token");
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}

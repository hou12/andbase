package com.cocolove2.andbase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.cocolover2.andbase.mvpbase.AndBaseMVPActivity;
import com.cocolover2.andbase.mvpbase.AndBasePresenter;
import com.cocolover2.andbase.mvpbase.IAndBaseMVPView;

/**
 * 界面基类
 * Created by liubo on 8/23/16.
 */
public class BaseActivity<V extends IAndBaseMVPView, P extends AndBasePresenter<V>> extends AndBaseMVPActivity<V, P> {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addActivity(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeActivity(this);
    }

    @Override
    protected P createPresenter() {
        return null;
    }

}

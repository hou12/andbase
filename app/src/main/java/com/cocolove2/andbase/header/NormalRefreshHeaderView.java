package com.cocolove2.andbase.header;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.cocolove2.andbase.R;
import com.cocolover2.andbase.utils.log.KLog;
import com.cocolover2.andbase.widget.refresh.OnSwipeTriggerListener;

/**
 * Created by cocolove2 on 17/6/8.
 */

public class NormalRefreshHeaderView extends FrameLayout implements OnSwipeTriggerListener {
    private TextView mHeaderStatusTv;
    private ImageView mHeaderArrowIv;
    private ImageView mHeaderChrysanthemumIv;
    private AnimationDrawable mHeaderChrysanthemumAd;
    private RotateAnimation mUpAnim;
    private RotateAnimation mDownAnim;

    private String mPullDownRefreshText = "下拉刷新";
    private String mReleaseRefreshText = "释放更新";
    private String mRefreshingText = "加载中...";

    public NormalRefreshHeaderView(Context context) {
        super(context);
        init(context);
    }

    public NormalRefreshHeaderView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public NormalRefreshHeaderView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void initAnimation() {
        mUpAnim = new RotateAnimation(0, -180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mUpAnim.setDuration(150);
        mUpAnim.setFillAfter(true);

        mDownAnim = new RotateAnimation(-180, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        mDownAnim.setDuration(150);
        mDownAnim.setFillAfter(true);
    }

    private void init(Context context) {
        initAnimation();
        setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        View view = LayoutInflater.from(context).inflate(R.layout.view_refresh_header_normal, this, false);
        mHeaderStatusTv = (TextView) view.findViewById(R.id.tv_normal_refresh_header_status);
        mHeaderArrowIv = (ImageView) view.findViewById(R.id.iv_normal_refresh_header_arrow);
        mHeaderChrysanthemumIv = (ImageView) view.findViewById(R.id.iv_normal_refresh_header_chrysanthemum);
        mHeaderChrysanthemumAd = (AnimationDrawable) mHeaderChrysanthemumIv.getDrawable();
        mHeaderStatusTv.setText(mPullDownRefreshText);
        addView(view);
    }


    @Override
    public void onMove(int scrollY, float mRefreshTriggerOffset, boolean isRefreshing, boolean automatic) {
        if (isRefreshing)
            return;

        if (scrollY < mRefreshTriggerOffset) {
            if (!mHeaderStatusTv.getText().equals(mPullDownRefreshText)) {
                mHeaderStatusTv.setText(mPullDownRefreshText);
                mHeaderChrysanthemumIv.setVisibility(View.INVISIBLE);
                mHeaderChrysanthemumAd.stop();
                mHeaderArrowIv.setVisibility(View.VISIBLE);
                mHeaderArrowIv.startAnimation(mDownAnim);
            }
        } else {
            if (!mHeaderStatusTv.getText().equals(mReleaseRefreshText)) {
                mHeaderStatusTv.setText(mReleaseRefreshText);
                mHeaderChrysanthemumIv.setVisibility(View.INVISIBLE);
                mHeaderChrysanthemumAd.stop();
                mHeaderArrowIv.setVisibility(View.VISIBLE);
                mHeaderArrowIv.startAnimation(mUpAnim);
            }
        }
    }

    @Override
    public void onRelease() {
        KLog.i("=================onRelease");
        mHeaderStatusTv.setText(mReleaseRefreshText);
        mHeaderChrysanthemumIv.setVisibility(View.INVISIBLE);
        mHeaderChrysanthemumAd.stop();
        mHeaderArrowIv.setVisibility(View.VISIBLE);
//        mHeaderArrowIv.startAnimation(mUpAnim);
    }

    @Override
    public void onRefresh() {
        KLog.i("===============onRefresh");
        mHeaderStatusTv.setText(mRefreshingText);
        // 必须把动画清空才能隐藏成功
        mHeaderArrowIv.clearAnimation();
        mHeaderArrowIv.setVisibility(View.INVISIBLE);
        mHeaderChrysanthemumIv.setVisibility(View.VISIBLE);
        mHeaderChrysanthemumAd.start();
    }

    @Override
    public void onComplete(String statusMsg) {
        KLog.i("===============onComplete");
        mHeaderStatusTv.setText(mPullDownRefreshText);
        mHeaderChrysanthemumIv.setVisibility(View.INVISIBLE);
        mHeaderChrysanthemumAd.stop();
        mHeaderArrowIv.setVisibility(View.VISIBLE);
        mDownAnim.setDuration(0);
        mHeaderArrowIv.startAnimation(mDownAnim);
    }

    @Override
    public void onReset() {
        KLog.i("===============onReset");
    }
}

package com.cocolove2.andbase.http;

import com.cocolove2.andbase.bean.Weather;
import com.cocolover2.andbase.mvpbase.IAndBaseMVPView;


public interface IHttpView extends IAndBaseMVPView {

    void onBaiduResult(String data, int code, String msg);

    void onWeatherInfo(Weather weather, int code, String msg);

    void onDownloadInfo(String path, int code, String msg);

    void onUploadInfo(String result, int code, String msg);
}

package com.cocolove2.andbase.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cocolove2.andbase.R;
import com.cocolove2.andbase.bean.City;
import com.cocolover2.andbase.utils.log.KLog;
import com.cocolover2.andbase.widget.SideIndexBar;
import com.github.promeg.pinyinhelper.Pinyin;
import com.github.promeg.tinypinyin.lexicons.android.cncity.CnCityDict;

import java.util.ArrayList;
import java.util.List;

/**
 * 侧边栏字母索引测试
 * Created by cocolove2 on 17/9/26.
 */

public class SldeIndexBarActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    SideIndexBar mIndexBar;
    List<City> mDatas = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index_bar);
        Pinyin.init(Pinyin.newConfig().with(CnCityDict.getInstance(this)));
        String[] citys = getResources().getStringArray(R.array.city_array);
        for (String s : citys) {
            City city = new City();
            city.setName(s);
            city.setFirstLetter(Pinyin.toPinyin(s.charAt(0)).substring(0, 1));
            mDatas.add(city);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.index_bar_rv);
        mIndexBar = (SideIndexBar) findViewById(R.id.index_bar_sidebar);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new MyAdapter());

        mIndexBar.setOnLetterChangedListener(new SideIndexBar.OnLetterChangedListener() {
            @Override
            public void onChanged(String s, int position) {
                KLog.i(s + "--------" + position);
            }

            @Override
            public void onCancel() {
                KLog.i("取消");
            }
        });
    }


    private class MyAdapter extends RecyclerView.Adapter<MyAdapter.VH> {

        @Override
        public VH onCreateViewHolder(ViewGroup parent, int viewType) {
            return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false));
        }

        @Override
        public void onBindViewHolder(VH holder, int position) {
            holder.tv.setText(mDatas.get(position).getName());
        }

        @Override
        public int getItemCount() {
            return mDatas.size();
        }

        class VH extends RecyclerView.ViewHolder {
            TextView tv;

            public VH(View itemView) {
                super(itemView);
                tv = (TextView) itemView.findViewById(R.id.item_city_tv);
            }
        }
    }
}

package com.cocolove2.andbase.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.cocolove2.andbase.R;
import com.cocolover2.andbase.caches.MulitipleCaches;

/**
 * Created by cocolove2 on 17/8/16.
 */

public class TestCacheActivity extends AppCompatActivity {

    final String key = "key";
    final String defValue = "value";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MulitipleCaches.getInstance().saveToMemory(key, "这是日志内容-merory");
        MulitipleCaches.getInstance().saveToDisk(this, key, "这是日志内容-disk", "cache1");
        MulitipleCaches.getInstance().saveToSp(this, key, "这是日志内容-sp");

        String a = MulitipleCaches.getInstance().getFromMemory(key, defValue).toString();
        String b = MulitipleCaches.getInstance().getFromDisk(this, key, defValue, "cache1").toString();
        String c = MulitipleCaches.getInstance().getFromSp(this, key, defValue).toString();
        Log.e("TEST", a + "#" + b + "#" + c + "#" + MulitipleCaches.getInstance().getDiskCachePath(this, "cache1"));
        MulitipleCaches.getInstance().removeMemory(key);
        MulitipleCaches.getInstance().removeDisk(this, key, "cache");
        MulitipleCaches.getInstance().removeSp(this, key);
        a = MulitipleCaches.getInstance().getFromMemory(key, defValue).toString();
        b = MulitipleCaches.getInstance().getFromDisk(this, key, defValue, "cache").toString();
        c = MulitipleCaches.getInstance().getFromSp(this, key, defValue).toString();
        Log.e("TEST", a + "#" + b + "#" + c);

    }

}

package com.cocolove2.andbase.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;

import com.cocolove2.andbase.R;
import com.cocolover2.andbase.utils.log.KLog;
import com.cocolover2.andbase.widget.SuperEditText;

/**
 * Created by cocolove2 on 17/6/16.
 */

public class SuperEditTextActivity extends AppCompatActivity {
    boolean isEyeShow;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_edit);
        final SuperEditText et = (SuperEditText) findViewById(R.id.super_et);
        et.setTextColor(Color.BLUE);
        et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        et.setSelection(et.getText().length());
        et.setEditFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                KLog.i("==========" + hasFocus);
            }
        });

        et.addTextWatcher(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                KLog.i("================" + s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et.setEditClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KLog.i("=========onClick11111");
            }
        });

        et.setExendClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEyeShow = !isEyeShow;
                et.setExtendResource(isEyeShow ? R.drawable.login_ic_pwd_show : R.drawable.login_ic_pwd_hide);
                if (isEyeShow) {
                    et.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    et.setSelection(et.getText().length());
                } else {
                    et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    et.setSelection(et.getText().length());
                }
            }
        });
    }
}

package com.cocolove2.andbase.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cocolove2.andbase.R;
import com.cocolove2.andbase.header.NormalRefreshHeaderView;
import com.cocolover2.andbase.widget.refresh.OnRefreshListener;
import com.cocolover2.andbase.widget.refresh.PullRefreshLayout;
import com.cocolover2.andbase.widget.refresh.header.CircleRefreshHeaderView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cocolove2 on 17/6/8.
 */

public class RefreshActivity extends AppCompatActivity implements OnRefreshListener {
    PullRefreshLayout mPullRefreshLayout;
    RecyclerView mRecyclerView;
    List<String> mDatas = new ArrayList<>();
    MyAdapter mMyAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refresh);

        for (int i = 0; i < 10; i++) {
            mDatas.add("#########" + i + "###########");
        }

        mPullRefreshLayout = (PullRefreshLayout) findViewById(R.id.refresh_pull);
        mRecyclerView = (RecyclerView) findViewById(R.id.pullrefresh_targetview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mMyAdapter = new MyAdapter();
        mRecyclerView.setAdapter(mMyAdapter);
        mPullRefreshLayout.setOnRefreshListener(this);
        mPullRefreshLayout.setRefreshHeaderView(new NormalRefreshHeaderView(this));


    }

    @Override
    public void onRefresh() {
        mPullRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mDatas.add("######### ADD ###########");
                mDatas.add("######### ADD ###########");
                mDatas.add("######### ADD ###########");
                mPullRefreshLayout.refreshCompleted("加载成功");
                mMyAdapter.notifyDataSetChanged();
            }
        }, 3000);


    }

    private class MyAdapter extends RecyclerView.Adapter<VH> {


        @Override
        public VH onCreateViewHolder(ViewGroup parent, int viewType) {
            return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_refresh, parent, false));
        }

        @Override
        public void onBindViewHolder(VH holder, int position) {
            holder.tv.setText(mDatas.get(position));
        }

        @Override
        public int getItemCount() {
            return mDatas.size();
        }
    }

    static class VH extends RecyclerView.ViewHolder {
        TextView tv;

        public VH(View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.item_txt);

        }
    }
}

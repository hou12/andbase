package com.cocolove2.andbase;

import com.cocolover2.andbase.caches.MulitipleCaches;

/**
 * 在这里进行本地数据读写操作
 * 该类中放置公共的持久化数据，单个模块私有的数据请自己保存，维护
 * Created by liubo on 10/2/16.
 */
@SuppressWarnings("all")
public class DaoHelper {


    private DaoHelper() {

    }

    public static DaoHelper getInstance() {
        return DaoHelperHolder.INSTANCE;
    }

    private static class DaoHelperHolder {
        private static final DaoHelper INSTANCE = new DaoHelper();
    }

    //token
    public void saveToken(String token) {
        MulitipleCaches.getInstance().saveToSp(MyApplication.getContext(), "token", token);
    }

    public void clearToken() {
        MulitipleCaches.getInstance().removeSp(MyApplication.getContext(), "token");
    }

    public String getToken() {
        return MulitipleCaches.getInstance().getFromSp(MyApplication.getContext(), "token", "").toString();
    }

    //帐号
    public void saveAccount(String account) {
        MulitipleCaches.getInstance().saveToSp(MyApplication.getContext(), "account", account);
    }

    public String getAccount() {
        return MulitipleCaches.getInstance().getFromSp(MyApplication.getContext(), "account", "").toString();
    }


}
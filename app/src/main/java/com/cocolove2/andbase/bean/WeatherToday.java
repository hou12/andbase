package com.cocolove2.andbase.bean;

/**
 * "today":{
 * "temperature":"8℃~11℃",
 * "weather":"小雨转阴",
 * "weather_id":{
 * "fa":"07",
 * "fb":"02"
 * },
 * "wind":"持续无风向微风",
 * "week":"星期三",
 * "city":"厦门",
 * "date_y":"2018年01月31日",
 * "dressing_index":"较冷",
 * "dressing_advice":"建议着厚外套加毛衣等服装。年老体弱者宜着大衣、呢外套加羊毛衫。",
 * "uv_index":"最弱",
 * "comfort_index":"",
 * "wash_index":"不宜",
 * "travel_index":"较不宜",
 * "exercise_index":"较不宜",
 * "drying_index":""
 * }
 * Created by cocolove2 on 18/1/31.
 */

public class WeatherToday {

    private String temperature;
    private String weather;
    private WeatherId weather_id;
    private String wind;
    private String week;
    private String city;
    private String date_y;
    private String dressing_index;
    private String dressing_advice;
    private String uv_index;

    public String getTemperature() {
        return temperature;
    }

    public String getWeather() {
        return weather;
    }

    public WeatherId getWeather_id() {
        return weather_id;
    }

    public String getWind() {
        return wind;
    }

    public String getWeek() {
        return week;
    }

    public String getCity() {
        return city;
    }

    public String getDate_y() {
        return date_y;
    }

    public String getDressing_index() {
        return dressing_index;
    }

    public String getDressing_advice() {
        return dressing_advice;
    }

    public String getUv_index() {
        return uv_index;
    }
}

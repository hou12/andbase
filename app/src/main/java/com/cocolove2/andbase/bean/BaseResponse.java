package com.cocolove2.andbase.bean;

import com.cocolover2.andbase.http.IResponseResult;
import com.cocolover2.andbase.utils.log.KLog;
import com.google.gson.annotations.SerializedName;

/**
 * 基本的响应类（所有的实体类都应继承该类）
 * json格式为{
 * data:{},
 * errcode:0,
 * errmsg:"xxxx"
 * }
 * Created by liubo on 1/6/17.
 */
@SuppressWarnings("all")
public class BaseResponse<T> implements IResponseResult {
    private static final String TAG = "BaseResponse";
    @SerializedName(value = "errcode", alternate = {"error_code"})
    private int errcode;
    @SerializedName(value = "errmsg", alternate = {"reason"})
    private String errmsg;
    @SerializedName(value = "data", alternate = {"result"})
    private T data;

    public T getData() {
        if (data == null) KLog.e(TAG, "接口数据返回的data对象为空");
        return data;
    }

    @Override
    public int getCode() {
        return errcode;
    }

    @Override
    public String getMessage() {
        return errmsg;
    }
}

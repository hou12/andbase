package com.cocolove2.andbase.bean;

/**
 * Created by cocolove2 on 17/9/26.
 */

public class City {
    private String name;
    private String firstLetter;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstLetter() {
        return firstLetter;
    }

    public void setFirstLetter(String firstLetter) {
        this.firstLetter = firstLetter;
    }
}

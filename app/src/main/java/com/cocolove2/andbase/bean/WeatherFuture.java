package com.cocolove2.andbase.bean;

/**
 * "temperature":"8℃~11℃",
 * "weather":"小雨转阴",
 * "weather_id":{
 * "fa":"07",
 * "fb":"02"
 * },
 * "wind":"持续无风向微风",
 * "week":"星期三",
 * "date":"20180131"
 * Created by cocolove2 on 18/1/31.
 */

public class WeatherFuture {
    private String temperature;
    private String weather;
    private WeatherId weather_id;
    private String wind;
    private String week;
    private String date;

    public String getTemperature() {
        return temperature;
    }

    public String getWeather() {
        return weather;
    }

    public WeatherId getWeather_id() {
        return weather_id;
    }

    public String getWind() {
        return wind;
    }

    public String getWeek() {
        return week;
    }

    public String getDate() {
        return date;
    }
}

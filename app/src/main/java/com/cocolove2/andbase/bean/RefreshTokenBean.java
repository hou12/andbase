package com.cocolove2.andbase.bean;

/**
 * token刷新的实体类
 * Created by cocolove2 on 17/7/20.
 */

public class RefreshTokenBean {
    private String token;
    private String description;
    private int code;
    private long validtime;

    public String getToken() {
        return token;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

    public long getValidtime() {
        return validtime;
    }
}

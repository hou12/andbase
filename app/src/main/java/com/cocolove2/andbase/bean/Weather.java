package com.cocolove2.andbase.bean;

import java.util.List;

/**
 * 天气接口
 * Created by cocolove2 on 18/1/31.
 */

public class Weather {
    private WeatherSk sk;
    private WeatherToday today;
    private List<WeatherFuture> future;

    public WeatherSk getSk() {
        return sk;
    }

    public WeatherToday getToday() {
        return today;
    }

    public List<WeatherFuture> getFuture() {
        return future;
    }
}

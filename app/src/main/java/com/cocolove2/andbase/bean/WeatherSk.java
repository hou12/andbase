package com.cocolove2.andbase.bean;

/**
 * "sk":{
 * "temp":"9",
 * "wind_direction":"北风",
 * "wind_strength":"3级",
 * "humidity":"99%",
 * "time":"20:33"
 * }
 * Created by cocolove2 on 18/1/31.
 */

public class WeatherSk {
    private String temp;
    private String wind_direction;
    private String wind_strength;
    private String humidity;
    private String time;

    public String getTemp() {
        return temp;
    }

    public String getWind_direction() {
        return wind_direction;
    }

    public String getWind_strength() {
        return wind_strength;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getTime() {
        return time;
    }
}

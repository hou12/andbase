package com.cocolove2.andbase;

import com.cocolove2.andbase.bean.BaseResponse;
import com.cocolove2.andbase.bean.RefreshTokenBean;
import com.cocolove2.andbase.bean.Weather;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * 这个也是网络请求接口模版,开发者可以根据自己的实际需求自定义
 * Created by liubo on 10/1/16.
 */

public interface ApiService {
    //=====================================测试=================================================
    //    @Headers("Cache-Control: max-age=640000")
//    @Headers({"cookie: cookiedasdasd", "Cache-Control: max-age=640000"})
    @POST("jsonObject")
    @FormUrlEncoded
    public Observable<ResponseBody> doTestUpload(@FieldMap Map<String, Object> params);


    /**
     * 发送验证码
     */
    @POST("http://121.43.122.35:8088/yjtx/wd/business/business!getValidateCode.json")
    @Deprecated
    @FormUrlEncoded
    Observable<ResponseBody> sendVerifyCode(@FieldMap Map<String, Object> params);


    @GET("http://www.baidu.com/")
    public Observable<ResponseBody> getBaidu();

    /**
     * 获取天气
     *
     * @param params
     * @return
     */
    @GET("https://v.juhe.cn/weather/index")
    public Observable<BaseResponse<Weather>> getWeather(@QueryMap Map<String, String> params);

    /**
     * 获取天气
     *
     * @param params
     * @return
     */
    @GET("https://v.juhe.cn/weather/index")
    public Observable<ResponseBody> getWeather2(@QueryMap Map<String, String> params);
    /**
     * 刷新token
     */
    @POST("xxxxxx")
    @FormUrlEncoded
    public Observable<BaseResponse<RefreshTokenBean>> refreshToken(@FieldMap Map<String, String> params);

    /**
     * 发送get请求
     *
     * @param url 请求路径
     */
    @GET
    Observable<ResponseBody> httpGet(@Url String url);

    /**
     * 发送post请求
     *
     * @param url     请求路径
     * @param headers 头部
     * @param params  表单
     */
    @POST
    @FormUrlEncoded
    Observable<ResponseBody> httpPost(@Url String url, @HeaderMap Map<String, String> headers, @FieldMap Map<String, String> params);


}

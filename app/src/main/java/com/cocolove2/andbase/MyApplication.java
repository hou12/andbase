package com.cocolove2.andbase;

import android.app.Application;
import android.content.Context;

/**
 * Created by cocolove2 on 17/5/31.
 */

public class MyApplication extends Application {
    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
    }

    public static Context getContext() {
        return sContext;
    }
}

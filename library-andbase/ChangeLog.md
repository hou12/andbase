

##2.1.7`2017-12-21`
* BaseApi支持`ResponseBody`返回接口支持多种协议错误码和关键字处理
* http框架加入对ResponseBody的统一处理
* 加入新的缓存管理 `MulitipleCaches`
* 加入recyclerview 点击监听
* 修复http文件下载进度回调bug.
* ImageUtil工具类中加入图片旋转功能

##2.1.2 `2017-09-28`

* 加入AudioHelper 工具类
* 调整playmanger工具类
* 加入 SideIndexbar控件
* 加入 ShimmerFrameLayout控件

##2.1.1 `2017-09-1`

* 调整DateTimeUtil.strToDate方法
* 1.mvp中的module抽象删除 2.调整媒体工具类的位置
* 加入websocket
* 更换网络请求框架，调整网络请求接口(大修改)`rxjava2`
* 调整网络异常处理方式
* 加入简单的文件上传和文件下载
## V2.1.0 `2017-08-05` 

* 删除运行时权限库
* 修复ListItemDecoration 划线bug.
* 更新rxjava+retrofit2+okhttp到2.x系列
* 修复该库中的部分bug.
* 调整网络请求部分的实现和方法(大改动)
* 删除mvp中的module抽象

```
//依赖库
 compile 'com.android.support:appcompat-v7:25.3.1'
    compile 'com.android.support:recyclerview-v7:25.3.1'
    compile 'com.android.support:design:25.3.1'

    compile 'com.squareup.okhttp3:okhttp:3.8.1'
    //rxjava2.x
    compile "io.reactivex.rxjava2:rxjava:2.1.3"
    compile 'io.reactivex.rxjava2:rxandroid:2.0.1'
    //retrofit2.x
    compile 'com.squareup.retrofit2:adapter-rxjava2:2.3.0'
    compile 'com.squareup.retrofit2:converter-gson:2.3.0'
    compile "com.squareup.retrofit2:retrofit:2.3.0"

```
##V2.0.4 `2017-06-17`

* 加入自定义控件`SuperEditText`


##V2.0.2 `2017-06-08`

* 引入角标提示库
* 优化刷新控件`pullRefreshLayout`
* 优化运行时权限判断`PermissionManager`

##版本V2.0.1 `2017-06-01`

* MediaPlayerHelper支持播放的音频的流的类型
* 调整`ABaseApi`，使用更加灵活

```
ext {
    minSdkVersion = 19
    targetSdkVersion = 25
    compileSdkVersion = 25
    buildToolsVersion = "25.0.2"

    // App dependencies
    supportLibVersion = "25.3.1"
    junitVersion = '4.12'
    okhttpVersion = "3.7.0"
    rxjava1xVersion = "1.2.9"
    rxandroid1xVersion = "1.2.1"
    retrofit2Version = "2.2.0"

    mockitoVersion = '1.10.19'
    powerMockito = '1.6.2'
    hamcrestVersion = '1.3'
    runnerVersion = '0.5'
    rulesVersion = '0.5'
    espressoVersion = '2.2.2'

    dexmakerVersion = '1.2'


}
```

## 版本V2.0.0 `2017-03-22`

* 删除DispalyItemable接口
* 删除AndBaseActivity中的运行时权限验证
* AndBaseModel加入更多方法,开放更多权限
* ABaseApi支持json格式网络请求传参
* 删除MLoger工具类建议使用 KLog

```
ext {
    minSdkVersion = 19
    targetSdkVersion = 25
    compileSdkVersion = 25
    buildToolsVersion = "25.0.2"

    // App dependencies
    supportLibVersion = "25.3.0"
    junitVersion = '4.12'
    okhttpVersion = "3.7.0"
    rxjava1xVersion = "1.2.9"
    rxandroid1xVersion = "1.2.1"
    retrofit2Version = "2.2.0"
}

```







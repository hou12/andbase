package com.cocolover2.andbase.http.ws;

import okhttp3.Response;
import okio.ByteString;

/**
 * @author rabtman 可用于监听ws连接状态并进一步拓展
 */
public abstract class WsStatusListener {

    public void onOpen(Response response) {
    }

    public void onMessage(String text) {
    }

    public void onMessage(ByteString bytes) {
    }

    /**
     * @param retryCount 重试次数
     * @return true 继续重连；false 停止重连，并重置次数
     */
    public boolean onReconnect(int retryCount) {
        return true;
    }

    public void onClosing(int code, String reason) {
    }


    public void onClosed(int code, String reason) {
    }

    public void onFailure(Throwable t, Response response) {
    }
}

package com.cocolover2.andbase.http.interceptor;


import android.text.TextUtils;

import com.cocolover2.andbase.utils.log.KLog;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Arrays;

import okhttp3.Connection;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * okhttp的日志拦截器
 * Created by liubo on 8/15/16.
 */
@SuppressWarnings("all")
public class LoggingInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        long t1 = System.nanoTime();
        logForRequest(request, chain.connection());
        Response response = chain.proceed(request);
        long t2 = System.nanoTime();
        return logForResponse(response, t1, t2);
    }


    private Response logForResponse(Response response, long start, long end) {
        try {
            KLog.w("##===============================response Log=======================================##");
            Response.Builder builder = response.newBuilder();
            Response clone = builder.build();
            KLog.i("response: " + URLDecoder.decode(clone.request().url().toString()) + String.format("  in %.1fms", (end - start) / 1e6d));

            Headers headers = clone.headers();
            for (int i = 0; i < headers.size(); i++) {
                KLog.i(headers.name(i) + ": " + headers.value(i));
            }
            KLog.i("code : " + clone.code());
            KLog.i("protocol : " + clone.protocol());
            if (!TextUtils.isEmpty(clone.message()))
                KLog.i("message : " + clone.message());

            ResponseBody body = clone.body();
            if (body != null) {
                MediaType mediaType = body.contentType();
                if (mediaType != null) {
                    KLog.i("responseBody's contentType : " + mediaType.toString());
                    if (isContentCanPrint(mediaType)) {
                        String resp = body.string();
                        KLog.i("responseBody's content : ");
                        KLog.json(resp);
                        body = ResponseBody.create(mediaType, resp);
                        return response.newBuilder().body(body).build();
                    } else {
                        KLog.i("responseBody's content : " + " maybe [file part] , too large too print , ignored!");
                    }
                }
            }
        } catch (Exception e) {
            KLog.e(e.getMessage());
        }
        return response;
    }


    private boolean isContentCanPrint(MediaType mediaType) {
        if (mediaType.type() != null && mediaType.type().equals("text")) {
            return true;
        }
        final String[] subTypeArrays = new String[]{"json", "xml", "html", "webviewhtml", "x-www-form-urlencoded"};
        return mediaType.subtype() != null && Arrays.asList(subTypeArrays).contains(mediaType.subtype());
    }


    private  void logForRequest(Request request, Connection connection) {
        try {
            KLog.w("##===============================request Log=======================================##");
            Headers headers = request.headers();
            KLog.i("request: " + URLDecoder.decode(request.url().toString()) + "  on " + connection);
            KLog.i("method: " + request.method());
            for (int i = 0; i < headers.size(); i++) {
                KLog.i(headers.name(i) + ": " + headers.value(i));
            }
            RequestBody requestBody = request.body();
            if (requestBody != null) {
                MediaType mediaType = requestBody.contentType();
                if (mediaType != null) {
                    KLog.i("requestBody's contentType : " + mediaType.toString());
                    if (isContentCanPrint(mediaType)) {
                        KLog.i("requestBody's content : ");
                        KLog.i(bodyToString(request).replace("&", "\n"));
                    } else {
                        KLog.i("requestBody's content : " + " maybe [file part] , too large too print , ignored!");
                    }
                }
            }
        } catch (Exception e) {
            KLog.e(e.getMessage());
        }
    }

    private String bodyToString(final Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return URLDecoder.decode(buffer.readUtf8());
        } catch (final IOException e) {
            return "something error when show requestBody.";
        }
    }
}

package com.cocolover2.andbase.http;


public abstract class OnHttpListener<T> {


    /**
     * 网络请求成功回调
     * <p>
     * 当返回的不是json字符串时，请求结果都走该接口
     * </p>
     *
     * @param response 1.返回实体类必须实现{@link IResponseResult}
     *                 2.返回对象{@link okhttp3.ResponseBody}
     */
    public abstract void onSuccess(T response);

    /**
     * 网络请求失败回调
     *
     * @param code 错误码
     * @param msg  错误信息
     */
    public void onFailure(int code, String msg) {
    }

    /**
     * 文件下载进度回调
     *
     * @param totalBytesRead 下载量
     * @param totalLength    总量
     * @param progress       下载进度
     */
    public void onDownloadProgress(long totalBytesRead, long totalLength, int progress) {

    }

    /**
     * 上传文件进度回调(执行于子线程)
     *
     * @param bytesWritten  上传的大小
     * @param contentLength 文件的总大小
     * @param networkSpeed  上传速度
     */
    public void onUploadProgress(long bytesWritten, long contentLength, long networkSpeed) {
    }

}

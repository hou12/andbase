package com.cocolover2.andbase.http.interceptor;

import android.content.Context;

import com.cocolover2.andbase.utils.NetworkUtil;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * okhttp网络缓存拦截器
 */
@Deprecated
public class CacheInterceptor implements Interceptor {
    private Context mContext;

    public CacheInterceptor(Context context) {
        mContext = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();
//        if (!NetworkUtil.isNetworkConnected(mContext)) {
//            request = request.newBuilder()
//                    .cacheControl(CacheControl.FORCE_CACHE)
//                    .build();
//        }

        Response originalResponse = chain.proceed(request);
        if (NetworkUtil.isNetworkConnected(mContext)) {
            //有网的时候读接口上的@Headers里的配置，你可以在这里进行统一的设置(注掉部分)
            String cacheControl = request.cacheControl().toString();
            return originalResponse.newBuilder()
                    .header("Cache-Control", cacheControl)
                    .removeHeader("Pragma") // 清除头信息，因为服务器如果不支持，会返回一些干扰信息，不清除下面无法生效
                    .build();
        }
        return originalResponse;
//        } else {
//            //没有网络是读取本地缓存数据
//            return originalResponse.newBuilder()
//                    .header("Cache-Control", "public, only-if-cached")
//                    .removeHeader("Pragma")
//                    .build();
//        }
    }


}
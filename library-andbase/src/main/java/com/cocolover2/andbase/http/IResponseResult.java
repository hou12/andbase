package com.cocolover2.andbase.http;


/**
 * 网络请求响应结果
 * Created by liubo on 8/24/16.
 */
public interface IResponseResult {
    /**
     * 获取请求响应码
     */
    int getCode();

    /**
     * 获取响应信息
     */
    String getMessage();
}

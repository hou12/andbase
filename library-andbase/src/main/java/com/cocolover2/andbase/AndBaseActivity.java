package com.cocolover2.andbase;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

/**
 * 基本Activity
 */
@SuppressWarnings("unused")
public abstract class AndBaseActivity extends AppCompatActivity {


    public void addActivity(Activity activity) {
        ActivityContainer.getInstance().addActivity(activity);
    }

    public void removeActivity(Activity activity) {
        ActivityContainer.getInstance().removeActivity(activity);
    }

    /**
     * 结束所有Activity
     */
    public void killAllActivity() {
        ActivityContainer.getInstance().killAllActivity();
    }

    public void killAllActivityExceptMe(Activity me) {
        ActivityContainer.getInstance().killAllActivityExceptMe(me);
    }


    public void killProcess() {
        // 杀死该应用进程
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}

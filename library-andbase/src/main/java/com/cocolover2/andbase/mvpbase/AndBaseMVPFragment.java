package com.cocolover2.andbase.mvpbase;

import android.os.Bundle;

import com.cocolover2.andbase.AndBaseFragment;

/**
 * <p>
 * 功能同{@link AndBaseMVPActivity}
 * </p>
 *
 * @param <V>
 * @param <P>
 * @author liubo
 */
public abstract class AndBaseMVPFragment<V extends IAndBaseMVPView, P extends AndBasePresenter<V>> extends AndBaseFragment implements IAndBaseMVPView {

    protected P mPresenter;


    @SuppressWarnings("unchecked")
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = createPresenter();
        if (mPresenter != null)
            mPresenter.attachView((V) this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPresenter != null)
            mPresenter.detachView();
    }

    protected abstract P createPresenter();
}

package com.cocolover2.andbase.mvpbase;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.cocolover2.andbase.AndBaseActivity;

/**
 * <p>
 * 针对MVP模式使用时,防止由于activity的声明周期造成的内存泄漏
 * </p>
 *
 * @param <V> 代表View接口
 * @param <P> 代表代理presenter
 */
public abstract class AndBaseMVPActivity<V extends IAndBaseMVPView, P extends AndBasePresenter<V>> extends AndBaseActivity implements IAndBaseMVPView {

    protected P mPresenter;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = createPresenter();
        if (mPresenter != null)
            mPresenter.attachView((V) this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null)
            mPresenter.detachView();
    }

    protected abstract P createPresenter();
}

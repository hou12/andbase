package com.cocolover2.andbase.adapterdelegates;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

/**
 *
 * 提供通用的{@link RecyclerViewHolder}方便使用
 *
 * Created by liubo on 5/24/16.
 */
public abstract class AbsSampleAdapterDelegate<I extends T,T> extends AbsAdapterDelegateBase<I,T,RecyclerViewHolder> {
    private Context mContext;
    private int itemLayoutId;

    public AbsSampleAdapterDelegate(Context context, int itemLayoutId, int viewType) {
        super(viewType);
        mContext=context;
        this.itemLayoutId=itemLayoutId;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new RecyclerViewHolder(LayoutInflater.from(mContext).inflate(itemLayoutId, parent, false));
    }

}

package com.cocolover2.andbase.adapterdelegates;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;


/**
 * 适用于RecyclerView的ViewHolder
 */
public class RecyclerViewHolder extends RecyclerView.ViewHolder {

    /* 缓存子视图,key为view id, 值为View。*/
    private SparseArray<View> mCahceViews = new SparseArray<View>();
    View mItemView;

    public RecyclerViewHolder(View itemView) {
        super(itemView);
        mItemView = itemView;
    }


    public Context getContext() {
        return mItemView.getContext();
    }


    /**
     * @param viewId
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T extends View> T findViewById(int viewId) {
        View target = mCahceViews.get(viewId);
        if (target == null) {
            target = mItemView.findViewById(viewId);
            mCahceViews.put(viewId, target);
        }
        return (T) target;
    }


    public void setText(int viewId, int stringId) {
        TextView textView = findViewById(viewId);
        textView.setText(stringId);
    }

    public void setText(int viewId, String text) {
        TextView textView = findViewById(viewId);
        textView.setText(text);
    }

    public void setTextColor(int viewId, int color) {
        TextView textView = findViewById(viewId);
        textView.setTextColor(color);
    }

    public void setBackgroundColor(int viewId, int color) {
        View target = findViewById(viewId);
        target.setBackgroundColor(color);

    }


    public void setBackgroundResource(int viewId, int resId) {
        View target = findViewById(viewId);
        target.setBackgroundResource(resId);
    }

    public void setBackgroundDrawable(int viewId, Drawable drawable) {
        View target = findViewById(viewId);
        target.setBackgroundDrawable(drawable);
    }

    @TargetApi(16)
    public void setBackground(int viewId, Drawable drawable) {
        View target = findViewById(viewId);
        target.setBackground(drawable);
    }


    public void setImageBitmap(int viewId, Bitmap bitmap) {
        ImageView target = findViewById(viewId);
        target.setImageBitmap(bitmap);
    }


    public void setImageResource(int viewId, int resId) {
        ImageView target = findViewById(viewId);
        target.setImageResource(resId);
    }


    public void setImageDrawable(int viewId, Drawable drawable) {
        ImageView target = findViewById(viewId);
        target.setImageDrawable(drawable);
    }


    public void setImageDrawable(int viewId, Uri uri) {
        ImageView target = findViewById(viewId);
        target.setImageURI(uri);
    }


    @TargetApi(16)
    public void setImageAlpha(int viewId, int alpha) {
        ImageView target = findViewById(viewId);
        target.setImageAlpha(alpha);
    }

    public void setChecked(int viewId, boolean checked) {
        Checkable checkable = findViewById(viewId);
        checkable.setChecked(checked);
    }


    public void setProgress(int viewId, int progress) {
        ProgressBar view = findViewById(viewId);
        view.setProgress(progress);
    }

    public void setProgress(int viewId, int progress, int max) {
        ProgressBar view = findViewById(viewId);
        view.setMax(max);
        view.setProgress(progress);
    }

    public void setMax(int viewId, int max) {
        ProgressBar view = findViewById(viewId);
        view.setMax(max);
    }

    public void setRating(int viewId, float rating) {
        RatingBar view = findViewById(viewId);
        view.setRating(rating);
    }

    public void setVisibility(int viewId, int visible) {
        View view = findViewById(viewId);
        view.setVisibility(visible);
    }


    public void setRating(int viewId, float rating, int max) {
        RatingBar view = findViewById(viewId);
        view.setMax(max);
        view.setRating(rating);
    }

}

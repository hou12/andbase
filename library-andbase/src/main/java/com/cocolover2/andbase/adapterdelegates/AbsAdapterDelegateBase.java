package com.cocolover2.andbase.adapterdelegates;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

/**
 * 适配器代表的基本类
 *
 * @param <I>  The type of the item that is managed by this AdapterDelegate. Must be a subtype of T
 * @param <T>  The generic type of the list, in other words: {@code List<T>}
 * @param <VH> The type of the ViewHolder
 */
public abstract class AbsAdapterDelegateBase<I extends T, T, VH extends RecyclerView.ViewHolder>
        implements AdapterDelegate<List<T>> {

    protected int viewType;

    public AbsAdapterDelegateBase(int viewType) {
        this.viewType = viewType;
    }

    @Override
    public int getItemViewType() {
        return viewType;
    }


    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(@NonNull List<T> items, int position, @NonNull RecyclerView.ViewHolder holder) {
        onBindData((VH) holder, position, (I) items.get(position));
        setupItemClickListener((VH) holder, position);
        setUpItemLongClickListener((VH) holder, position);
    }

    public abstract void onBindData(VH holder, int position, I item);

    protected void setupItemClickListener(VH viewHolder, final int position) {
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClicked(viewType, position);
                }
            }
        });

    }

    protected void setUpItemLongClickListener(VH viewHolder, final int position) {
        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mOnItemLongClickListener != null)
                    mOnItemLongClickListener.onItemLongClicked(viewType, position);
                return false;
            }
        });
    }

    protected OnItemClikListener mOnItemClickListener;
    protected OnItemLongClickListener mOnItemLongClickListener;

    public void setOnItemClickListener(OnItemClikListener listener) {
        mOnItemClickListener = listener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
        mOnItemLongClickListener = listener;
    }

    public interface OnItemClikListener {
        void onItemClicked(int viewType, int position);
    }

    public interface OnItemLongClickListener {
        void onItemLongClicked(int viewType, int postion);
    }
}

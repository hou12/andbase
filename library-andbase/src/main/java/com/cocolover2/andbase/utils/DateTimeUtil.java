package com.cocolover2.andbase.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@Deprecated
public class DateTimeUtil {
    public static final long DAY_TIME_MILLIS = 24 * 60 * 60 * 1000;// 一天的毫秒值
    public static final long WEEK_TIME_MILLIS = 7 * DAY_TIME_MILLIS;// 一周的毫秒值
    public static final long HOUR_TIME_MILLIS = 60 * 60 * 1000;// 一小时的毫秒值
    public static final long MINUTE_TIME_MILLIS = 60 * 1000;// 一分钟的毫秒值


    public static final String HOUR_PATTERN = "HH:mm";// 小时格式

    public static final String YESTERDAY_STYLE_1 = "昨天 HH:mm";
    public static final String YESTERDAY_STYLE_2 = "昨天";

    public static final String THE_DAY_BEFORE_YESTERDAY_STYLE_1 = "前天 HH:mm";
    public static final String THE_DAY_BEFORE_YESTERDAY_STYLE_2 = "前天";

    public static final String WEEK_STYLE_1 = "EE HH:mm";// 星期几
    public static final String WEEK_STYLE_2 = "EE";// 星期几

    //常用的日期格式
    public static final String DATE_STYLE_1 = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_STYLE_2 = "yyyy-MM-dd HH:mm";
    public static final String DATE_STYLE_3 = "yyyy-MM-dd";
    public static final String DATE_STYLE_4 = "MM-dd";
    public static final String DATE_STYLE_5 = "yy/MM/dd";
    public static final String DATE_STYLE_6 = "yyyyMMddHHmmss";


    /**
     * 显示日期 (如:刚刚,几分钟前等)
     *
     * @param oldTime     创建时间
     * @param currentTime 系统当前时间
     */
    public static String showDateTime(long oldTime, long currentTime) {
        long time = Math.abs(currentTime - oldTime);
        if (time < MINUTE_TIME_MILLIS) {// 小于1分钟
            return "刚刚";
        }
        if (time < HOUR_TIME_MILLIS) {// 小于1小时
            return time / MINUTE_TIME_MILLIS + "分钟前";
        }
        if (time < DAY_TIME_MILLIS) {// 小于1天
            return time / HOUR_TIME_MILLIS + "小时前";
        }
        if (time < WEEK_TIME_MILLIS) {// 少于1周
            return time / DAY_TIME_MILLIS + "天前";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_STYLE_4, Locale.CHINA);
        return sdf.format(oldTime);
    }

    /**
     * 显示日期 (如:昨天,前天,星期几等)
     *
     * @param createTime  创建时间
     * @param currentTime 系统当前时间
     */
    public static String showDateTime2(long createTime, long currentTime) {

        SimpleDateFormat df = new SimpleDateFormat(DATE_STYLE_1, Locale.CHINA);
        long times = currentTime / DAY_TIME_MILLIS - createTime / DAY_TIME_MILLIS;
        if (times < 1) {// 时间小于一天
            df.applyPattern(HOUR_PATTERN);
        } else if (times < 2) {
            df.applyPattern(YESTERDAY_STYLE_2);
        } else if (times < 3) {
            df.applyPattern(THE_DAY_BEFORE_YESTERDAY_STYLE_2);
        } else if (times < 7) {
            df.applyPattern(WEEK_STYLE_2);
        } else {
            df.applyPattern(DATE_STYLE_5);
        }
        return df.format(new Date(createTime));
    }

    /**
     * 显示日期 (如:昨天 HH:mm,前天 HH:mm,星期几 HH:mm 等)
     *
     * @param createTime  创建时间
     * @param currentTime 系统当前时间
     */
    public static String showDateTime3(long createTime, long currentTime) {

        SimpleDateFormat df = new SimpleDateFormat(DATE_STYLE_1, Locale.CHINA);
        long times = currentTime / DAY_TIME_MILLIS - createTime / DAY_TIME_MILLIS;
        if (times < 1) {// 时间小于一天
            df.applyPattern(HOUR_PATTERN);
        } else if (times < 2) {
            df.applyPattern(YESTERDAY_STYLE_1);
        } else if (times < 3) {
            df.applyPattern(THE_DAY_BEFORE_YESTERDAY_STYLE_1);
        } else if (times < 7) {
            df.applyPattern(WEEK_STYLE_1);
        } else {
            df.applyPattern(DATE_STYLE_2);
        }
        return df.format(new Date(createTime));
    }


    /**
     * 将字符串转化为时间
     */
    public static Date strToDate(String str, String pattern) {
        // sample：Tue May 31 17:46:55 +0800 2011
        // E：周 MMM：字符串形式的月，如果只有两个M，表示数值形式的月 Z表示时区（＋0800）
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.CHINA);
        Date result = null;
        try {
            result = sdf.parse(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 将时长(单位:秒)，转换为几分几秒，适用于通话时长之类的，如02:30
     */
    public static String convertCallTime(long sec) {
        if (sec < 0)
            return String.valueOf(sec);

        StringBuilder buf = new StringBuilder();
        if (sec > 60) {
            int min = (int) (sec / 60);
            int second = (int) (sec % 60);
            if (min < 10)
                buf.append("0");
            buf.append(min).append(":");

            if (second < 10)
                buf.append("0");
            buf.append(second);
        } else {
            buf.append("00:");
            if (sec < 10)
                buf.append("0");
            buf.append(sec);
        }

        return buf.toString();
    }

    /**
     * 将时长(单位:秒)，转换为几分几秒，适用于通话时长之类的，如2'30''
     */
    public static String convertBetweenLen(long sec) {
        if (sec < 0)
            return String.valueOf(sec);

        StringBuilder buf = new StringBuilder();
        if (sec > 60) {
            int min = (int) (sec / 60);
            int second = (int) (sec % 60);
            buf.append(min).append("'").append(second).append("''");
        } else {
            buf.append(sec).append("''");
        }

        return buf.toString();
    }

    /**
     * 农历日历,以及天干地支,生肖计算帮助类
     */
    public static class Lunar {
        public int year;
        public int month;
        public int day;
        public boolean leap;//是否是闰年

        public final static String chineseNumber[] = {"一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"};
        final static SimpleDateFormat chineseDateFormat = new SimpleDateFormat("yyyy年MM月dd日", Locale.CHINA);
        final static long[] lunarInfo = new long[]{0x04bd8, 0x04ae0, 0x0a570, 0x054d5, 0x0d260, 0x0d950, 0x16554, 0x056a0,
                0x09ad0, 0x055d2, 0x04ae0, 0x0a5b6, 0x0a4d0, 0x0d250, 0x1d255, 0x0b540, 0x0d6a0, 0x0ada2, 0x095b0, 0x14977,
                0x04970, 0x0a4b0, 0x0b4b5, 0x06a50, 0x06d40, 0x1ab54, 0x02b60, 0x09570, 0x052f2, 0x04970, 0x06566, 0x0d4a0,
                0x0ea50, 0x06e95, 0x05ad0, 0x02b60, 0x186e3, 0x092e0, 0x1c8d7, 0x0c950, 0x0d4a0, 0x1d8a6, 0x0b550, 0x056a0,
                0x1a5b4, 0x025d0, 0x092d0, 0x0d2b2, 0x0a950, 0x0b557, 0x06ca0, 0x0b550, 0x15355, 0x04da0, 0x0a5d0, 0x14573,
                0x052d0, 0x0a9a8, 0x0e950, 0x06aa0, 0x0aea6, 0x0ab50, 0x04b60, 0x0aae4, 0x0a570, 0x05260, 0x0f263, 0x0d950,
                0x05b57, 0x056a0, 0x096d0, 0x04dd5, 0x04ad0, 0x0a4d0, 0x0d4d4, 0x0d250, 0x0d558, 0x0b540, 0x0b5a0, 0x195a6,
                0x095b0, 0x049b0, 0x0a974, 0x0a4b0, 0x0b27a, 0x06a50, 0x06d40, 0x0af46, 0x0ab60, 0x09570, 0x04af5, 0x04970,
                0x064b0, 0x074a3, 0x0ea50, 0x06b58, 0x055c0, 0x0ab60, 0x096d5, 0x092e0, 0x0c960, 0x0d954, 0x0d4a0, 0x0da50,
                0x07552, 0x056a0, 0x0abb7, 0x025d0, 0x092d0, 0x0cab5, 0x0a950, 0x0b4a0, 0x0baa4, 0x0ad50, 0x055d9, 0x04ba0,
                0x0a5b0, 0x15176, 0x052b0, 0x0a930, 0x07954, 0x06aa0, 0x0ad50, 0x05b52, 0x04b60, 0x0a6e6, 0x0a4e0, 0x0d260,
                0x0ea65, 0x0d530, 0x05aa0, 0x076a3, 0x096d0, 0x04bd7, 0x04ad0, 0x0a4d0, 0x1d0b6, 0x0d250, 0x0d520, 0x0dd45,
                0x0b5a0, 0x056d0, 0x055b2, 0x049b0, 0x0a577, 0x0a4b0, 0x0aa50, 0x1b255, 0x06d20, 0x0ada0};

        // ====== 传回农历 y年的总天数
        private static int yearDays(int y) {
            int i, sum = 348;
            for (i = 0x8000; i > 0x8; i >>= 1) {
                if ((lunarInfo[y - 1900] & i) != 0)
                    sum += 1;
            }
            return (sum + leapDays(y));
        }

        // ====== 传回农历 y年闰月的天数
        private static int leapDays(int y) {
            if (leapMonth(y) != 0) {
                if ((lunarInfo[y - 1900] & 0x10000) != 0)
                    return 30;
                else
                    return 29;
            } else
                return 0;
        }

        // ====== 传回农历 y年闰哪个月 1-12 , 没闰传回 0
        private static int leapMonth(int y) {
            return (int) (lunarInfo[y - 1900] & 0xf);
        }

        // ====== 传回农历 y年m月的总天数
        private static int monthDays(int y, int m) {
            if ((lunarInfo[y - 1900] & (0x10000 >> m)) == 0)
                return 29;
            else
                return 30;
        }

        // ====== 传回农历 y年的生肖
        public String animalsYear() {
            final String[] Animals = new String[]{"鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪"};
            return Animals[(year - 4) % 12];
        }

        // ====== 传入 月日的offset 传回干支, 0=甲子
        public String cyclicalm(int num) {
            final String[] Gan = new String[]{"甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸"};
            final String[] Zhi = new String[]{"子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥"};
            return (Gan[num % 10] + Zhi[num % 12]);
        }

        // ====== 传入 offset 传回干支, 0=甲子
        public String cyclical() {
            int num = year - 1900 + 36;
            return (cyclicalm(num));
        }

        /**
         * 传出y年m月d日对应的农历. yearCyl3:农历年与1864的相差数 ? monCyl4:从1900年1月31日以来,闰月数
         * dayCyl5:与1900年1月31日相差的天数,再加40 ?
         *
         * @param cal
         */
        public Lunar(Calendar cal) {
            Date baseDate = null;
            try {
                baseDate = chineseDateFormat.parse("1900年1月31日");
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // 求出和1900年1月31日相差的天数
            int offset = (int) ((cal.getTime().getTime() - baseDate.getTime()) / 86400000L);
            int monCyl = 14;

            // 用offset减去每农历年的天数
            // 计算当天是农历第几天
            // i最终结果是农历的年份
            // offset是当年的第几天
            int iYear, daysOfYear = 0;
            for (iYear = 1900; iYear < 2050 && offset > 0; iYear++) {
                daysOfYear = yearDays(iYear);
                offset -= daysOfYear;
                monCyl += 12;
            }
            if (offset < 0) {
                offset += daysOfYear;
                iYear--;
                monCyl -= 12;
            }
            // 农历年份
            year = iYear;

            int leapMonth = leapMonth(iYear); // 闰哪个月,1-12
            leap = false;

            // 用当年的天数offset,逐个减去每月（农历）的天数，求出当天是本月的第几天
            int iMonth, daysOfMonth = 0;
            for (iMonth = 1; iMonth < 13 && offset > 0; iMonth++) {
                // 闰月
                if (leapMonth > 0 && iMonth == (leapMonth + 1) && !leap) {
                    --iMonth;
                    leap = true;
                    daysOfMonth = leapDays(year);
                } else
                    daysOfMonth = monthDays(year, iMonth);

                offset -= daysOfMonth;
                // 解除闰月
                if (leap && iMonth == (leapMonth + 1))
                    leap = false;
                if (!leap)
                    monCyl++;
            }
            // offset为0时，并且刚才计算的月份是闰月，要校正
            if (offset == 0 && leapMonth > 0 && iMonth == leapMonth + 1) {
                if (leap) {
                    leap = false;
                } else {
                    leap = true;
                    --iMonth;
                    --monCyl;
                }
            }
            // offset小于0时，也要校正
            if (offset < 0) {
                offset += daysOfMonth;
                --iMonth;
                --monCyl;
            }
            month = iMonth;
            day = offset + 1;
        }

        public String getChinaDayString(int day) {
            String chineseTen[] = {"初", "十", "廿", "卅"};
            int n = day % 10 == 0 ? 9 : day % 10 - 1;
            if (day > 30)
                return "";
            if (day == 10)
                return "初十";
            else
                return chineseTen[day / 10] + chineseNumber[n];
        }


        public String toString() {
            return year + "年" + (leap ? "闰" : "") + chineseNumber[month - 1] + "月" + getChinaDayString(day);
        }

    }
}

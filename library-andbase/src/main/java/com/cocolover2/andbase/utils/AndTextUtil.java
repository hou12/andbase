package com.cocolover2.andbase.utils;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;

import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 文本数据处理工具类
 *
 * @since 1.1.1
 */
@Deprecated
public class AndTextUtil {
    private AndTextUtil() {
        throw new UnsupportedOperationException("cannot be instantiated");
    }


    /**
     * 判断输入是否符合给定的格式
     *
     * @param pattern 格式
     * @param input   输入的数据
     * @return true 匹配, false 不匹配
     */
    public static boolean patternRegexString(String pattern, String input) {
        Pattern _pattern = Pattern.compile(pattern);
        Matcher matcher = _pattern.matcher(input);
        return matcher.find();
    }


    /**
     * 格式化文件大小
     *
     * @param size 单位:比特
     * @return 格式化后的字符串
     */
    public static String formatFileSize(long size) {
        DecimalFormat df = new DecimalFormat(".00");// 保留两位小数
        if (size < 1024)
            return size + "B";
        if (size < 1024 * 1024)
            return df.format(size / 1024.0) + "KB";
        if (size < 1024 * 1024 * 1024L)
            return df.format(size / (1024.0 * 1024.0)) + "MB";
        return df.format(size / (1024.0 * 1024.0 * 1024.0)) + "TB";
    }

    /**
     * 获取MD5加密后的字符串
     *
     * @param origString 未加密的字符串
     */
    @Deprecated
    public static String getMD5ofStr(String origString) {
        String origMD5 = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] result = md5.digest(origString.getBytes());
            origMD5 = byteArray2HexStr(result);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return origMD5;
    }

    private static String byteArray2HexStr(byte[] bs) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bs) {
            sb.append(byte2HexStr(b));
        }
        return sb.toString();
    }

    private static String byte2HexStr(byte b) {
        String hexStr = null;
        int n = b;
        if (n < 0) {
            // 若需要自定义加密,请修改这个移位算法即可
            n = b & 0x7F + 128;
        }
        hexStr = Integer.toHexString(n / 16) + Integer.toHexString(n % 16);
        return hexStr.toUpperCase();
    }


    /**
     * 获取指定关键字标色的文本
     *
     * @param text           原文
     * @param specifiedTexts 要标色的文本
     * @param color          颜色
     * @return 处理后的文本
     */
    public static SpannableStringBuilder getSpecifiedTextsColor(String text, String specifiedTexts, int color) {
        if (TextUtils.isEmpty(specifiedTexts)) {
            return new SpannableStringBuilder(text);
        }

        ArrayList<Integer> sTextsStartList = new ArrayList<>();

        int sTextLength = specifiedTexts.length();
        String temp = text;
        int lengthFront = 0;
        int start = -1;
        do {
            start = temp.indexOf(specifiedTexts);
            if (start != -1) {
                start = start + lengthFront;
                sTextsStartList.add(start);
                lengthFront = start + sTextLength;
                temp = text.substring(lengthFront);
            }

        } while (start != -1);

        SpannableStringBuilder styledText = new SpannableStringBuilder(text);
        for (Integer i : sTextsStartList) {
            styledText.setSpan(
                    new ForegroundColorSpan(color),
                    i,
                    i + sTextLength,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return styledText;
    }

}

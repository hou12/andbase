package com.cocolover2.andbase.utils.encrypt;

import java.security.MessageDigest;

/**
 * 加密解密帮助类
 * Created by liubo on 3/9/17.
 */

public class EncryptUtil {

    public static String enctryptAES(String content, String password) {
        return AESUtil.enctrypt(content, password);
    }
    public static String decryptAES(byte[] content, String password){
        return AESUtil.decrypt(content,password);
    }

    /**
     * MD5加密字符串
     *
     * @param origString 未加密的字符串
     */
    public static String enctryMD5(String origString) {
        String origMD5 = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] result = md5.digest(origString.getBytes());
            origMD5 = byteArray2HexStr(result);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return origMD5;
    }

    private static String byteArray2HexStr(byte[] bs) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bs) {
            sb.append(byte2HexStr(b));
        }
        return sb.toString();
    }

    private static String byte2HexStr(byte b) {
        String hexStr = null;
        int n = b;
        if (n < 0) {
            // 若需要自定义加密,请修改这个移位算法即可
            n = b & 0x7F + 128;
        }
        hexStr = Integer.toHexString(n / 16) + Integer.toHexString(n % 16);
        return hexStr.toUpperCase();
    }


}

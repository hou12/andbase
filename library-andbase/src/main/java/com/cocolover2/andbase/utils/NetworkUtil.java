package com.cocolover2.andbase.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;

@SuppressWarnings("all")
public final class NetworkUtil {
    private static final String TAG = "NetworkUtil";

    public static final String NET_NO = "NO";
    public static final String NET_WIFI = "WIFI";
    public static final String NET_2G = "2G";
    public static final String NET_3G = "3G";
    public static final String NET_4G = "4G";

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null != connectivity) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (null != info && info.isConnected()) {
                if (info.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * 获取当前网络是否可用(true:isAvailable; false:unAvailable)
     *
     * @param pContext Context
     * @return true:isAvailable; false:otherwise;
     */
    public static boolean isNetworkAvailable(Context pContext) {
        if (pContext != null) {
            ConnectivityManager _CM = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (_CM != null) {
                try {
                    if (_CM.getActiveNetworkInfo() != null) {
                        if (_CM.getActiveNetworkInfo().isAvailable()) {
                            return true;
                        }
                    }
                } catch (Exception e) {
                    return false;
                }

            }
            return false;
        }
        return false;
    }

    /**
     * 获取Wifi是否可用
     *
     * @param pContext Context
     * @return true:isAvailable; false:otherwise;
     */
    public static boolean isWiFiActive(Context pContext) {
        if (pContext == null)
            return false;
        boolean _IsAvailable = false;
        WifiManager _WifiManager;
        WifiInfo _WifiInfo;
        try {
            _WifiManager = (WifiManager) pContext.getSystemService(Context.WIFI_SERVICE);
            _WifiInfo = _WifiManager.getConnectionInfo();
            int _IpAddress = _WifiInfo == null ? 0 : _WifiInfo.getIpAddress();
            if (_WifiManager.isWifiEnabled() && _IpAddress != 0) {
                _IsAvailable = true;
            }
        } catch (Exception e) {
            _IsAvailable = false;
        }
        return _IsAvailable;
    }

    /**
     * 获取当前的网络名称(WIFI or 2G or 3G or 4G)
     *
     * @param pContext Context
     * @return WIFI or 2G or 3G or 4G
     */
    public static String getNetworkName(Context pContext) {
        try {
            ConnectivityManager _CM = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo _NetworkInfo = _CM.getActiveNetworkInfo();
            if (_NetworkInfo == null || !_NetworkInfo.isConnected()) {
                return NET_NO; //not connected
            }
            if (_NetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                return NET_WIFI;
            }
            if (_NetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                int networkType = _NetworkInfo.getSubtype();
                switch (networkType) {
                    case TelephonyManager.NETWORK_TYPE_GPRS:
                    case TelephonyManager.NETWORK_TYPE_EDGE:
                    case TelephonyManager.NETWORK_TYPE_CDMA:
                    case TelephonyManager.NETWORK_TYPE_1xRTT:
                    case TelephonyManager.NETWORK_TYPE_IDEN:
                        return NET_2G;
                    case TelephonyManager.NETWORK_TYPE_UMTS:
                    case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    case TelephonyManager.NETWORK_TYPE_HSDPA:
                    case TelephonyManager.NETWORK_TYPE_HSUPA:
                    case TelephonyManager.NETWORK_TYPE_HSPA:
                    case TelephonyManager.NETWORK_TYPE_EVDO_B:
                    case TelephonyManager.NETWORK_TYPE_EHRPD:
                    case TelephonyManager.NETWORK_TYPE_HSPAP:
                        return NET_3G;
                    case TelephonyManager.NETWORK_TYPE_LTE:
                        return NET_4G;
                }
            }
        } catch (Exception e) {
            return NET_NO;
        }
        return NET_NO;
    }

    /**
     * 获取 Wifi 名称
     *
     * @param pContext
     * @return
     */
    public static String getWifiName(Context pContext) {
        String _WifiName = "";
        WifiManager _WifiManager = (WifiManager) pContext.getSystemService(Context.WIFI_SERVICE);
        _WifiName = _WifiManager.getConnectionInfo().getSSID();
        return _WifiName;
    }

    /**
     * 获取 Wifi状态
     *
     * @param pContext Context
     * @return
     */
    public static int getWifiStatus(Context pContext) {
        return ((WifiManager) pContext.getSystemService(Context.WIFI_SERVICE)).getWifiState();
    }

    /**
     * 获取 Wifi IP
     *
     * @param pContext
     * @return
     */
    public static String getHostIp(Context pContext) {
        String _HostIp = "";
        try {
            WifiManager _WifiManager = (WifiManager) pContext.getSystemService(Context.WIFI_SERVICE);
            int i;
            if (_WifiManager.isWifiEnabled()) {
                i = _WifiManager.getConnectionInfo().getIpAddress();
                if (i != 0)
                    _HostIp = (i & 0xFF) + "." + (0xFF & i >> 8) + "."
                            + (0xFF & i >> 16) + "." + (0xFF & i >> 24);
            } else {
                i = getWifiStatus(pContext);
                if ((i == 2) || (i == 3)) {
                    Enumeration<?> _Enumeration = NetworkInterface.getNetworkInterfaces();
                    while (_Enumeration.hasMoreElements()) {
                        Enumeration<?> _EnumerationTemp = ((NetworkInterface) _Enumeration
                                .nextElement()).getInetAddresses();
                        if (!_EnumerationTemp.hasMoreElements())
                            ;
                        while (true) {
                            if (_HostIp.length() <= 0)
                                break;
                            InetAddress _LocalInetAddress = (InetAddress) _EnumerationTemp
                                    .nextElement();
                            if ((_LocalInetAddress.isLoopbackAddress())
                                    || (!(_LocalInetAddress instanceof Inet4Address)))
                                break;
                            _HostIp = _LocalInetAddress.getHostAddress();
                        }
                    }
                }
            }
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return _HostIp;
    }

    /**
     * 获取手机MAC地址
     *
     * @param pContext
     * @return
     */
    public static String getMacAddress(Context pContext) {
        String _MacAddress = "";
        try {
            WifiManager _WifiManager = (WifiManager) pContext.getSystemService(Context.WIFI_SERVICE);
            WifiInfo _WifiInfo = _WifiManager.getConnectionInfo();
            _MacAddress = _WifiInfo.getMacAddress();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _MacAddress;
    }

    /**
     * 获取手机搜索到的WIFI的BSSID
     *
     * @param pContext pContext
     * @return
     */
    public static String getScanBSSID(Context pContext) {
        String _MacAddress = "";
        StringBuffer _StringBuffer = new StringBuffer();
        try {
            WifiManager _WifiManager = (WifiManager) pContext.getSystemService(Context.WIFI_SERVICE);
            List<ScanResult> _ScanResults = _WifiManager.getScanResults();
            int i = 0;
            for (ScanResult _ScanResult : _ScanResults) {
                if (i < 15) {
                    _StringBuffer.append(_ScanResult.BSSID).append(",");
                    i++;
                }
            }
            if (_StringBuffer.length() > 0) {
                if ((_StringBuffer.charAt(_StringBuffer.length() - 1)) == ',') {
                    _StringBuffer.deleteCharAt(_StringBuffer.length() - 1);
                }
                _MacAddress = _StringBuffer.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _MacAddress;
    }

    /**
     * 获取URL的域名
     *
     * @param pUrl URL
     * @return host url
     */
    public static String getDomainOfUrl(String pUrl) {
        URL _URL;
        String _Host = "";
        try {
            _URL = new URL(pUrl);
            _Host = _URL.getHost();
        } catch (Exception e) {
        }
        return _Host;
    }
}

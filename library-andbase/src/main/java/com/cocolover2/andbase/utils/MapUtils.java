package com.cocolover2.andbase.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import java.net.URISyntaxException;
import java.util.List;

/**
 * 调用第三方地图工具类
 * Created by liubo on 9/21/16.
 */
@Deprecated
public class MapUtils {
    //高德,百度
    public static final String[] MAPS = {"com.autonavi.minimap", "com.baidu.BaiduMap"};

    /*
    * 检查手机上是否安装了指定的软件
    * @param context
    * @param packageName：应用包名
    * @return
    */
    public static boolean isAvilible(Context context, String packageName) {
        //获取packagemanager
        final PackageManager packageManager = context.getPackageManager();
        //获取所有已安装程序的包信息
        List<PackageInfo> packageInfos = packageManager.getInstalledPackages(0);
        for (PackageInfo pi : packageInfos) {
            if (pi.packageName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 调用手机上的百度地图进行导航
     */
    public static void naviUseBaiduMap(Context context, String appName, double latitude, double longitude) {
        final String uri = "intent://map/direction?"
                + "&destination=latlng:" + latitude + "," + longitude//目的地坐标
                + "&mode=transit"//公交导航
                + "coord_type=gcj02"//国标坐标
                + "&src=" + appName + "#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end";

        try {
            Intent intent = Intent.getIntent(uri);
            context.startActivity(intent); //启动调用
        } catch (URISyntaxException e) {
            Log.e("intent", e.getMessage());
        }
    }

    /**
     * 调用手机上的高德地图
     */
    public static void naviUseLBS(Context context, String app_name, double latitude, double longitude) {
        try {
            Intent intent = Intent.getIntent("androidamap://navi?sourceApplication=" + app_name + "&poiname=我的目的地&lat=" + latitude + "&lon=" + longitude + "&dev=0&style=2");
            context.startActivity(intent);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }
}

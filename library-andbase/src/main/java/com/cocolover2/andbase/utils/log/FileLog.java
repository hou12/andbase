package com.cocolover2.andbase.utils.log;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 在android手机上查询时，建议不要用htmlViewer查看，HTMLViewer查看UTF-8的中文部分会乱码，查看GBK的好像不会
 */
class FileLog {
    private static final String FILE_PREFIX = "KLog_";
    private static final String FILE_FORMAT = ".log";

    static void printFile(String tag, File targetDirectory, @Nullable String fileName, String headString, String msg) {
        fileName = (fileName == null) ? getFileName(tag) : fileName;
        if (save(targetDirectory, fileName, msg)) {
            Log.i(tag, headString + " save log success ! location is >>>" + targetDirectory.getAbsolutePath() + "/" + fileName);
        } else {
            Log.e(tag, headString + "save log fails !");
        }
    }

    private static boolean save(File dic, @NonNull String fileName, String msg) {
        File file = new File(dic, fileName);
        try {
            OutputStream outputStream = new FileOutputStream(file, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
            outputStreamWriter.write(msg);
            outputStreamWriter.flush();
            outputStream.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    private static String getFileName(String tag) {
        if (tag == null) tag = "";
        return FILE_PREFIX + new SimpleDateFormat("yyyy_MM_dd", Locale.CHINA).format(new Date()) + "_" + tag + FILE_FORMAT;
//        Random random = new Random();
//        return FILE_PREFIX + Long.toString(System.currentTimeMillis() + random.nextInt(10000)).substring(4) + FILE_FORMAT;
    }

}

package com.cocolover2.andbase.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;


public class KeyBoardUtil {

    private static boolean isFirst = true;

    public interface OnGetSoftHeightListener {
        void onShowed(int height);
    }

    public interface OnSoftKeyWordShowListener {
        void hasShow(boolean isShow);
    }


    /**
     * 获取软键盘的高度
     *
     * @param rootView 根布局
     * @param listener 软键盘弹出后高度的回调函数
     */
    public static void getSoftKeyboardHeight(final View rootView, final OnGetSoftHeightListener listener) {
        final ViewTreeObserver.OnGlobalLayoutListener layoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (isFirst) {
                    final Rect rect = new Rect();
                    rootView.getWindowVisibleDisplayFrame(rect);
                    //该方法获取的高度是全屏的高度
//                    final int screenHeight = rootView.getRootView().getHeight();
                    //该方法获取的高度不包括底部虚拟按键的高度
                    final int screenHeight = ((Activity) rootView.getContext()).getWindowManager().getDefaultDisplay().getHeight();

                    final int heightDifference = screenHeight - rect.bottom;
                    boolean visible = heightDifference > screenHeight / 3;
                    if (visible) {
                        isFirst = false;
                        if (listener != null) {
                            listener.onShowed(heightDifference);
                        }
                        rootView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                }
            }
        };
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(layoutListener);
    }

    /**
     * 判断软键盘是否弹出
     *
     * @param rootView 根布局
     * @param listener 软键盘是否弹出的回调
     */
    public static ViewTreeObserver.OnGlobalLayoutListener doMonitorSoftKeyWord(final View rootView, final OnSoftKeyWordShowListener listener) {
        final ViewTreeObserver.OnGlobalLayoutListener layoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                final Rect rect = new Rect();
                rootView.getWindowVisibleDisplayFrame(rect);
                final int screenHeight = rootView.getRootView().getHeight();
                final int heightDifference = screenHeight - rect.bottom;
                boolean visible = heightDifference > screenHeight / 3;
                if (listener != null)
                    listener.hasShow(visible);

            }
        };
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(layoutListener);
        return layoutListener;
    }

    /**
     * 隐藏软件盘
     *
     * @param activity 上下文环境
     * @param v        视图
     */
    public static void hideKeyboard(Activity activity, View v) {
        if (activity == null || v == null)
            return;
        InputMethodManager imm = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    /**
     * 隐藏软键盘
     */
    public static void hideKeyboard(Activity activity) {
        if (activity == null)
            return;
        InputMethodManager manager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null && manager.isActive())
            manager.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}

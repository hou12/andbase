package com.cocolover2.andbase.utils;

import android.content.Context;
import android.util.TypedValue;

public class DimensionUtil {

    private DimensionUtil() {
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static int spToPx(Context context, int spValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue,
                context.getResources().getDisplayMetrics());
    }


    public static int dpToPx(Context context, int dpValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dpValue, context.getResources().getDisplayMetrics());
    }
}

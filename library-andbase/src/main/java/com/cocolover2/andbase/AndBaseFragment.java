package com.cocolover2.andbase;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * 注意:<br>
 * firstFragment 显示出来时布局还没有初始化完，所以第一个fragment的lazyLoad方法没有调用<br>
 * 可以在第一个fragment布局初始化成功后手动调用
 *
 * @author liubo
 */
public abstract class AndBaseFragment extends Fragment {

    protected boolean isVisiable;
    protected boolean isPrepared;


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisiable = true;
            onVisible();
        } else {
            isVisiable = false;
            onInvisible();
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isPrepared = true;
    }

    public abstract void lazyLoad();

    protected void onVisible() {
        if (!isPrepared || !isVisiable) {
            return;
        }
        lazyLoad();
    }

    protected void onInvisible() {
    }
}

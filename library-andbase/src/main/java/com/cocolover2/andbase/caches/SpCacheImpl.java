package com.cocolover2.andbase.caches;

import android.content.Context;
import android.content.SharedPreferences;

import com.cocolover2.andbase.utils.encrypt.EncryptUtil;

/**
 * {@link android.content.SharedPreferences} 保存基本数据
 * Created by cocolove2 on 17/8/15.
 */

 class SpCacheImpl implements CocoCaches {
    private Context mContext;
    /**
     * 保存在手机里面的文件名
     */
    private static final String FILE_NAME = "cache_data";

    public SpCacheImpl(Context context) {
        mContext = context;
    }

    private String buildKey(String orignalKey) {
        return EncryptUtil.enctryMD5(orignalKey);
    }

    @Override
    public void saveToCache(String key, Object value) {
        if (mContext == null) {
            throw new NullPointerException("SharedPreferences context == null");
        }
        SharedPreferences sp = mContext.getApplicationContext().getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sp.edit();

        if (value instanceof String) {
            editor.putString(buildKey(key), (String) value);
        } else if (value instanceof Integer) {
            editor.putInt(buildKey(key), (Integer) value);
        } else if (value instanceof Boolean) {
            editor.putBoolean(buildKey(key), (Boolean) value);
        } else if (value instanceof Float) {
            editor.putFloat(buildKey(key), (Float) value);
        } else if (value instanceof Long) {
            editor.putLong(buildKey(key), (Long) value);
        } else {
            editor.putString(buildKey(key), value.toString());
        }
        editor.apply();
    }

    @Override
    public Object getFromCache(String key, Object defaultObject) {
        if (mContext == null) {
            throw new NullPointerException("SharedPreferences context == null");
        }
        SharedPreferences sp = mContext.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);

        if (defaultObject instanceof String) {
            return sp.getString(buildKey(key), (String) defaultObject);
        } else if (defaultObject instanceof Integer) {
            return sp.getInt(buildKey(key), (Integer) defaultObject);
        } else if (defaultObject instanceof Boolean) {
            return sp.getBoolean(buildKey(key), (Boolean) defaultObject);
        } else if (defaultObject instanceof Float) {
            return sp.getFloat(buildKey(key), (Float) defaultObject);
        } else if (defaultObject instanceof Long) {
            return sp.getLong(buildKey(key), (Long) defaultObject);
        }
        return defaultObject;
    }

    @Override
    public void removeFromCache(String key) {
        if (mContext == null) {
            throw new NullPointerException("SharedPreferences context == null");
        }
        SharedPreferences sp = mContext.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(buildKey(key));
        editor.apply();
    }

    @Override
    public void clearCache() {
        if (mContext == null) {
            throw new NullPointerException("SharedPreferences context == null");
        }
        SharedPreferences sp = mContext.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
    }
}

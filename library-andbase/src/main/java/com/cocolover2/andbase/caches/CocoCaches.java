package com.cocolover2.andbase.caches;

/**
 * 缓存接口
 * Created by cocolove2 on 17/8/15.
 */

 interface CocoCaches {

     void saveToCache(String key, Object value);

     Object getFromCache(String key, Object defaultObject);

     void removeFromCache(String key);

     void clearCache();
}

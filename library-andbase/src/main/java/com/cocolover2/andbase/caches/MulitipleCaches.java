package com.cocolover2.andbase.caches;

import android.content.Context;
import android.support.annotation.NonNull;

import com.cocolover2.andbase.utils.log.KLog;

import java.io.IOException;


/**
 * 缓存管理类
 * 1.内存缓存(bitmap/String)
 * 2.sd卡缓存(最大100M)
 * 3.SharedPreferences缓存
 *
 * @author cocolove2
 */
public final class MulitipleCaches {

    private DiskCacheImpl mDiskCache;
    private MemoryCacheImpl mMemoryCache;
    private SpCacheImpl mSpCache;

    private static class MulitipleCachesHolder {
        private static final MulitipleCaches INSTANCE = new MulitipleCaches();
    }

    public static MulitipleCaches getInstance() {
        return MulitipleCachesHolder.INSTANCE;
    }

    /**
     * @param context context
     * @param key     key
     * @param value   基本类型＋String
     */
    public void saveToSp(Context context, String key, Object value) {
        checkSpCache(context);
        mSpCache.saveToCache(key, value);
    }

    public Object getFromSp(Context context, String key, Object defValue) {
        checkSpCache(context);
        return mSpCache.getFromCache(key, defValue);
    }

    public void removeSp(Context context, String key) {
        checkSpCache(context);
        mSpCache.removeFromCache(key);
    }

    public void clearSp(Context context) {
        checkSpCache(context);
        mSpCache.clearCache();
    }

    /**
     * @param context context
     * @param key     key
     * @param value   Serializable 类型
     * @param dirName 文件夹名称
     */
    public void saveToDisk(Context context, String key, Object value, String dirName) {
        checkDiskCache(context, dirName);
        if (mDiskCache != null) {
            mDiskCache.saveToCache(key, value);
        }
    }

    public Object getFromDisk(Context context, String key, Object value, String dirName) {
        checkDiskCache(context, dirName);
        if (mDiskCache != null) {
            return mDiskCache.getFromCache(key, value);
        } else {
            return value;
        }
    }

    /**
     * @param context context
     * @param key     key
     * @param value   byte[] 类型
     * @param dirName 文件夹名称
     */
    public void saveToDisk(Context context, String key, byte[] value, String dirName) {
        checkDiskCache(context, dirName);
        if (mDiskCache != null) {
            mDiskCache.saveToCache(key, value);
        }
    }

    public byte[] getFromDisk(Context context, String key, String dirName) {
        checkDiskCache(context, dirName);
        if (mDiskCache != null) {
            return mDiskCache.getAsBytes(key);
        } else {
            return null;
        }
    }


    /**
     * 获取磁盘缓存路径
     *
     * @param context context
     * @param dirName 文件夹名称
     * @return 路径
     */
    public String getDiskCachePath(Context context, String dirName) {
        checkDiskCache(context, dirName);
        if (mDiskCache != null) {
            return mDiskCache.getCacheDir();
        } else {
            return "";
        }
    }

    public void removeDisk(Context context, String key, String dirName) {
        checkDiskCache(context, dirName);
        if (mDiskCache != null) {
            mDiskCache.removeFromCache(key);
        }
    }

    public void clearDisk(Context context, String dirName) {
        checkDiskCache(context, dirName);
        if (mDiskCache != null) {
            mDiskCache.clearCache();
        }
    }

    /**
     * @param key   key
     * @param value bitmap或字符串
     */
    public void saveToMemory(String key, Object value) {
        checkMemoryCache();
        mMemoryCache.saveToCache(key, value);
    }

    public Object getFromMemory(String key, Object defvalue) {
        checkMemoryCache();
        return mMemoryCache.getFromCache(key, defvalue);
    }

    public void removeMemory(String key) {
        checkMemoryCache();
        mMemoryCache.removeFromCache(key);
    }

    public void clearMemory() {
        checkMemoryCache();
        mMemoryCache.clearCache();
    }


    private void checkMemoryCache() {
        if (mMemoryCache == null) {
            mMemoryCache = new MemoryCacheImpl();
        }
    }

    private void checkDiskCache(Context context, @NonNull String dirName) {
        try {
            if (mDiskCache == null || !dirName.equals(mDiskCache.getDirName())) {
                mDiskCache = new DiskCacheImpl(context, dirName);
            }
        } catch (IOException e) {
            e.printStackTrace();
            KLog.e("===========>sd卡缓存实例创建失败");
        }
    }

    private void checkSpCache(Context context) {
        if (mSpCache == null) {
            mSpCache = new SpCacheImpl(context);
        }
    }


}

package com.cocolover2.andbase.media;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.TextUtils;

import com.cocolover2.andbase.utils.log.KLog;

import java.io.IOException;

/**
 * 媒体播放管理类
 * Created by cocolove2 on 17/8/5.
 */
public class PlayManager {
    private static final String TAG = "PlayManager";
    private MediaPlayer mediaPlayer;

    public static PlayManager getInstance() {
        return AudioPlayManagerHolder.INSTANCE;
    }


    private final static class AudioPlayManagerHolder {
        private final static PlayManager INSTANCE = new PlayManager();
    }

    @SuppressWarnings("all")
    public static class PlayCallBack {
        public void onPrepared(MediaPlayer mp) {
            mp.start();
            KLog.i(TAG, "========================onPrepared");
        }

        public void onCompletion(MediaPlayer mp) {
            KLog.i(TAG, "========================onCompletion");
        }

        public void onSeekComplete(MediaPlayer mp) {
            KLog.i(TAG, "========================onSeekComplete");
        }

        public void onError(MediaPlayer mp, int what, int extra) {
            KLog.e(TAG, "========================onError ==" + what + "#" + extra);
        }
    }

    /**
     * 初始化 播放器
     *
     * @param streamType audio流的类型{ AudioManager#STREAM_RING,AudioManager#STREAM_VOICE_CALL,AudioManager#STREAM_MUSIC}
     * @param callback   播放结果回调
     */
    private void init(int streamType, final PlayCallBack callback) {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
        }
        mediaPlayer.reset();
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                callback.onPrepared(mp);
            }
        });
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                callback.onCompletion(mp);
            }
        });
        mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {
                callback.onSeekComplete(mp);
            }
        });
        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                callback.onError(mp, what, extra);
                return false;
            }
        });
        mediaPlayer.setAudioStreamType(streamType);
    }

    /**
     * 停止播放
     */
    public void stop() {
        if (isPlaying()) {
            mediaPlayer.stop();
        }
    }

    /**
     * 暂停播放
     */
    public void pause() {
        if (isPlaying()) {
            mediaPlayer.pause();
        }
    }

    /**
     * 释放资源
     */
    public void release() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    /**
     * 是否正在播放
     *
     * @return 正在播放返回true, 否则返回false
     */
    public boolean isPlaying() {
        return mediaPlayer != null && mediaPlayer.isPlaying();
    }

    /**
     * 播放音频文件
     *
     * @param filePath   (file-path or http/rtsp URL) to use.
     * @param isLooping  是否循环播放
     * @param streamType audio流的类型{ AudioManager#STREAM_RING,AudioManager#STREAM_VOICE_CALL,AudioManager#STREAM_MUSIC}
     * @param callback   播放结果回调
     */
    public void playAudio(String filePath, boolean isLooping, int streamType, PlayCallBack callback) {
        init(streamType, callback == null ? new PlayCallBack() : callback);
        try {
            mediaPlayer.setDataSource(filePath);
            mediaPlayer.setLooping(isLooping);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playAudio(Context context, Uri uri, boolean isLooping, int streamType, PlayCallBack callback) {
        init(streamType, callback == null ? new PlayCallBack() : callback);
        try {
            mediaPlayer.setDataSource(context, uri);
            mediaPlayer.setLooping(isLooping);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 播放 assets文件夹下的音频资源
     *
     * @param context    上下文
     * @param assetName  音频资源名
     * @param isLooping  是否循环播放
     * @param streamType audio流的类型{AudioManager#STREAM_RING,AudioManager#STREAM_VOICE_CALL,AudioManager#STREAM_MUSIC}
     * @param callback   播放结果回调
     */
    public void playAssetsAudio(Context context, String assetName, boolean isLooping, int streamType, PlayCallBack callback) {
        if (context == null || TextUtils.isEmpty(assetName))
            return;
        init(streamType, callback);
        try {
            AssetFileDescriptor afd = context.getAssets().openFd(assetName);
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer.setLooping(isLooping);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

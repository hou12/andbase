package com.cocolover2.andbase.widget.refresh.header;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cocolover2.andbase.R;
import com.cocolover2.andbase.widget.refresh.OnSwipeTriggerListener;

/**
 * 带圆圈的下拉刷新的头部
 * Created by liubo on 6/16/16.
 */
public class CircleRefreshHeaderView extends LinearLayout implements OnSwipeTriggerListener {
    private RefreshCircleView mRefreshCircleView;
    private TextView mTextStatus;

    public CircleRefreshHeaderView(Context context) {
        super(context);
    }

    public CircleRefreshHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircleRefreshHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }



    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mRefreshCircleView = (RefreshCircleView) findViewById(R.id.layout_refresh_header_circle);
        mTextStatus = (TextView) findViewById(R.id.layout_refresh_header_text);
        mTextStatus.setText(R.string.refresh_drag_text);
    }


    @Override
    public void onMove(int y, float offset, boolean isRefreshing, boolean automatic) {
        if (isRefreshing)
            return;
        final int degree = y % 360;
        mRefreshCircleView.moveBall(degree);
        if (y > offset) {
            mTextStatus.setText(R.string.refresh_release_text);
        } else {
            mTextStatus.setText(R.string.refresh_drag_text);
        }

    }

    @Override
    public void onRelease() {
        mRefreshCircleView.startAnimtion();
    }

    @Override
    public void onRefresh() {
        mTextStatus.setText(R.string.refresh_refreshing);
        if (!mRefreshCircleView.isRunning())
            mRefreshCircleView.startAnimtion();
    }

    @Override
    public void onComplete(String statusMsg) {
        mRefreshCircleView.stopAnimation();
        mTextStatus.setText(statusMsg);
    }

    @Override
    public void onReset() {
        mTextStatus.setText(R.string.refresh_drag_text);
        if (mRefreshCircleView.isRunning())
            mRefreshCircleView.startAnimtion();
    }
}

package com.cocolover2.andbase.widget.recycler.itemdecoration;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * recyclerView的线性布局的分割线<br>
 * 1.可以使用主题中的listDivider属性进行设置<br>
 * 2.可以使用自定义drawable设置(优先级高于listDivider)
 * <pre>
 * 定义一个shape xml文件,示例如下
 * #solid android:color="#e1e1e1"
 * #size android:height="1px"
 * </pre>
 * {@link android.support.v7.widget.DividerItemDecoration}
 *
 * @since 1.1.1
 */
public class ListItemDecoration extends RecyclerView.ItemDecoration {

    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};

    public static final int HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL;

    public static final int VERTICAL_LIST = LinearLayoutManager.VERTICAL;


    private Drawable mDivider;

    private int mOrientation;
    private Context mContext;
    private int mLeftMargin;
    private int mRightMargin;

    public ListItemDecoration(Context context, int orientation) {
        this(context, orientation, null);
    }

    public ListItemDecoration(Context context, int orientation, Drawable drawable) {
        mContext = context;
        if (drawable == null) {
            final TypedArray a = context.obtainStyledAttributes(ATTRS);
            mDivider = a.getDrawable(0);
            a.recycle();
        } else
            mDivider = drawable;
        setOrientation(orientation);
    }

    public void setOrientation(int orientation) {
        if (orientation != HORIZONTAL_LIST && orientation != VERTICAL_LIST) {
            throw new IllegalArgumentException("invalid orientation");
        }
        mOrientation = orientation;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (mOrientation == VERTICAL_LIST) {
            drawVertical(c, parent);
        } else {
            drawHorizontal(c, parent);
        }
    }

    /**
     * 针对垂直分割线
     *
     * @since 1.3.2
     */
    public void setLeftMargin(int margin_dp) {
        mLeftMargin = (int) (mContext.getResources().getDisplayMetrics().density * margin_dp);
    }

    /**
     * 针对垂直分割线
     *
     * @since 1.3.2
     */
    public void setRightMargin(int margin_dp) {
        mRightMargin = (int) (mContext.getResources().getDisplayMetrics().density * margin_dp);
    }


    public void drawVertical(Canvas c, RecyclerView parent) {
        final int left = parent.getPaddingLeft() + mLeftMargin;
        final int right = parent.getWidth() - parent.getPaddingRight() - mRightMargin;

        final int childCount = parent.getChildCount();
//最后的一个分割线不用画
        for (int i = 0; i < childCount - 1; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int top = child.getBottom() + params.bottomMargin;
            final int bottom = top + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    public void drawHorizontal(Canvas c, RecyclerView parent) {
        final int top = parent.getPaddingTop();
        final int bottom = parent.getHeight() - parent.getPaddingBottom();

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int left = child.getRight() + params.rightMargin;
            final int right = left + mDivider.getIntrinsicWidth();
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    /**
     * 该方法用于当我们不想为item添加分割线时,可以设置相应的间距,如果不想设置间距,可以不用重写
     *
     * @param outRect
     * @param view
     * @param parent
     * @param state
     */
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (mOrientation == VERTICAL_LIST) {
            outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
        } else {
            outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
        }
    }
}

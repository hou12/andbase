package com.cocolover2.andbase.widget.clip;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.RelativeLayout;

/**
 * 图片裁剪的布局，支持圆形图片和正方形图片的裁剪
 */
public class ClipImageLayout extends RelativeLayout {

    private ClipZoomImageView mZoomImageView;//自定义图片缩放view
    private ClipImageBorderView mClipImageView;//自定义图片裁剪显示区域
    private int mHorizontalPadding = 60;

    public ClipImageLayout(Context context) {
        this(context, null);
    }

    public ClipImageLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mZoomImageView = new ClipZoomImageView(context);
        mClipImageView = new ClipImageBorderView(context);
        android.view.ViewGroup.LayoutParams lp = new LayoutParams(
                android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                android.view.ViewGroup.LayoutParams.MATCH_PARENT);
        this.addView(mZoomImageView, lp);
        this.addView(mClipImageView, lp);
        // 计算padding的px
        mHorizontalPadding = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, mHorizontalPadding, getResources()
                        .getDisplayMetrics());
        mZoomImageView.setHorizontalPadding(mHorizontalPadding);
        mClipImageView.setHorizontalPadding(mHorizontalPadding);
    }

    /**
     * 对外公布设置边距的方法,单位为dp
     */
    public void setHorizontalPadding(int horizontalPadding) {
        mHorizontalPadding = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, horizontalPadding, getResources()
                        .getDisplayMetrics());
        mZoomImageView.setHorizontalPadding(horizontalPadding);
        mClipImageView.setHorizontalPadding(horizontalPadding);
    }

    /**
     * 设置是否是圆形裁剪框
     * @param value true 圆形 false 矩形
     */
    public void setIsCircle(boolean value) {
        mClipImageView.setIsCircle(value);
    }

    /**
     * 裁切图片,并返回裁剪后的图片
     */
    public Bitmap clip() {
        return mZoomImageView.clip();
    }

    public void setBitmap(Bitmap bitmap) {
        mZoomImageView.setImageBitmap(bitmap);
    }

}

package com.cocolover2.andbase.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.cocolover2.andbase.R;

import java.util.Locale;

/**
 * 带有倒计时的button按钮，常用于发送验证码<br>
 * Created by Administrator on 2015/8/30.
 */
public class CountDownButton extends AppCompatButton {

    private static final int default_time = 60 * 1000;//默认一分钟

    private CountDownTimer countDownTimer;

    private int originalTextColor;
    private String originalTextStr;

    private int countDownTextColor;

    private Drawable originalBackground;
    private Drawable pressBackground;


    public CountDownButton(Context context) {
        super(context);
        init(context, null);
    }

    public CountDownButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CountDownButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.CountDownBtn);
        originalTextColor = array.getColor(R.styleable.CountDownBtn_orgin_text_color, 0x000000);
        countDownTextColor = array.getColor(R.styleable.CountDownBtn_press_text_color, getResources().getColor(android.R.color.darker_gray));
        originalBackground = array.getDrawable(R.styleable.CountDownBtn_orgin_background);
        pressBackground = array.getDrawable(R.styleable.CountDownBtn_press_background);
        array.recycle();
        setOriginalConfig();
    }

    /**
     * 启动倒计时
     *
     * @param times       times小于等于0时,使用默认时间60s  (单位:毫秒)
     * @param countFormat 格式
     */
    public void startCountDown(final int times, final String countFormat) {
        setEnabled(false);
        if (times <= 0)
            startCountDown2(default_time, countFormat);
        else
            startCountDown2(times, countFormat);
    }


    private void startCountDown2(int times, final String formatStr) {
        setBackground(pressBackground);
        setTextColor(countDownTextColor);

        if (countDownTimer == null) {
            countDownTimer = new CountDownTimer(times, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    int downTime = (int) millisUntilFinished / 1000;
                    if (formatStr == null || TextUtils.isEmpty(formatStr))
                        setText(String.format(Locale.CHINA, "重发(%1$02d)", downTime));
                    else
                        setText(String.format(formatStr, downTime));
                }

                @Override
                public void onFinish() {
                    reset();
                }
            };
        }
        countDownTimer.start();

    }

    private void setOriginalConfig() {
        if (originalTextStr == null && !TextUtils.isEmpty(getText()))
            originalTextStr = getText().toString();
        setText(originalTextStr);
        setBackground(originalBackground);
        setTextColor(originalTextColor);
    }

    public void reset() {
        setOriginalConfig();
        setEnabled(true);
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

}

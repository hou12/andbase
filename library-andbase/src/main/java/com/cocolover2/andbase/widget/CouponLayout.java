package com.cocolover2.andbase.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.cocolover2.andbase.R;


/**
 * 优惠券自定义布局
 * Created by liubo on 6/17/16.
 *
 * @since 1.1.1
 */
public class CouponLayout extends LinearLayout {

    private Paint mPaint;
    //间隙宽度
    private float gap = 8;
    //锯齿的半径
    private float sawtoothRadius = 10;
    private int sawtoothColor;
    private boolean mTopSawtoothEnable;
    private boolean mBottomSawtoothEnable;
    private boolean mLeftSawtoothEnable;
    private boolean mRightSawtoothEnable;


    private int mWidthCircleNum;
    private int mHeightCircleNum;
    //锯齿半圆画完后的剩余的宽度(单边)
    private float mWidthRemainSize;

    private float mHeightRemainSize;

    public CouponLayout(Context context) {
        super(context);
        init(context, null);
    }

    public CouponLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CouponLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.CouponLayout);

        try {
            gap = array.getDimension(R.styleable.CouponLayout_gap, 4);
            sawtoothRadius = array.getDimension(R.styleable.CouponLayout_sawtoothRadius, 3);

            mBottomSawtoothEnable = array.getBoolean(R.styleable.CouponLayout_bottomSawtoothEnable, false);
            mTopSawtoothEnable = array.getBoolean(R.styleable.CouponLayout_topSawtoothEnable, false);
            mLeftSawtoothEnable = array.getBoolean(R.styleable.CouponLayout_leftSawtoothEnable, false);
            mRightSawtoothEnable = array.getBoolean(R.styleable.CouponLayout_rightSawtoothEnable, false);

            sawtoothColor = array.getColor(R.styleable.CouponLayout_sawtoothColor, Color.WHITE);

            mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            //防止抖动
            mPaint.setDither(true);

            mPaint.setStyle(Paint.Style.FILL);

        } finally {
            array.recycle();
        }
    }

    /**
     * 设置锯齿的间距
     *
     * @param gap
     */
    public void setGap(float gap) {
        this.gap = gap;
    }

    public float getSawtoothRadius() {
        return sawtoothRadius;
    }

    /**
     * 设置锯齿的半径
     *
     * @param sawtoothRadius
     */
    public void setSawtoothRadius(float sawtoothRadius) {
        this.sawtoothRadius = sawtoothRadius;
    }

    public boolean isTopSawtoothEnable() {
        return mTopSawtoothEnable;
    }

    public void setTopSawtoothEnable(boolean topSawtoothEnable) {
        mTopSawtoothEnable = topSawtoothEnable;
    }

    public boolean isBottomSawtoothEnable() {
        return mBottomSawtoothEnable;
    }

    public void setBottomSawtoothEnable(boolean bottomSawtoothEnable) {
        mBottomSawtoothEnable = bottomSawtoothEnable;
    }

    public boolean isLeftSawtoothEnable() {
        return mLeftSawtoothEnable;
    }

    public void setLeftSawtoothEnable(boolean leftSawtoothEnable) {
        mLeftSawtoothEnable = leftSawtoothEnable;
    }

    public boolean isRightSawtoothEnable() {
        return mRightSawtoothEnable;
    }

    public void setRightSawtoothEnable(boolean rightSawtoothEnable) {
        mRightSawtoothEnable = rightSawtoothEnable;
    }

    public float getGap() {
        return gap;
    }

    /**
     * 获取锯齿的颜色
     *
     * @return
     */
    public int getSawtoothColor() {
        return sawtoothColor;
    }

    /**
     * 设置锯齿的颜色
     *
     * @param sawtoothColor
     */
    public void setSawtoothColor(int sawtoothColor) {
        this.sawtoothColor = sawtoothColor;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        //画锯齿时的宽度剩余的部分
        mWidthRemainSize = (w - gap) % (2 * sawtoothRadius + gap);
        mHeightRemainSize = (h - gap) % (2 * sawtoothRadius + gap);
        mWidthCircleNum = (int) ((w - gap) / (2 * sawtoothRadius + gap));
        mHeightCircleNum = (int) ((h - gap) / (2 * sawtoothRadius + gap));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint.setColor(sawtoothColor);
        if (mTopSawtoothEnable || mBottomSawtoothEnable) {
            float x = 0;
            for (int i = 0; i < mWidthCircleNum; i++) {
                x = gap + sawtoothRadius + mWidthRemainSize / 2 + ((gap + sawtoothRadius * 2) * i);
                if (mTopSawtoothEnable)
                    canvas.drawCircle(x, 0, sawtoothRadius, mPaint);
                if (mBottomSawtoothEnable)
                    canvas.drawCircle(x, getHeight(), sawtoothRadius, mPaint);
            }
        }

        if (mLeftSawtoothEnable || mRightSawtoothEnable) {
            float y = 0;
            for (int i = 0; i < mHeightCircleNum; i++) {
                y = gap + sawtoothRadius + mHeightRemainSize / 2 + ((gap + sawtoothRadius * 2) * i);
                if (mLeftSawtoothEnable)
                    canvas.drawCircle(0, y, sawtoothRadius, mPaint);
                if (mRightSawtoothEnable)
                    canvas.drawCircle(getWidth(), y, sawtoothRadius, mPaint);
            }
        }
    }
}

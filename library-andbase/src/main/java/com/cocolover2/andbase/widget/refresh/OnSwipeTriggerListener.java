package com.cocolover2.andbase.widget.refresh;

/**
 *
 * Created by liubo on 6/3/16.
 */
public interface OnSwipeTriggerListener {


    /**
     * 移动
     *
     * @param scrollY        y轴上移动的距离
     * @param slideThreshold 滑动的阈值
     * @param isRefreshing   是否正在刷新
     * @param automatic      是否是自动刷新
     */
    void onMove(int scrollY, float slideThreshold, boolean isRefreshing, boolean automatic);

    /**
     * 释放
     */
    void onRelease();

    /**
     * 正在刷新
     */
    void onRefresh();

    /**
     * 刷新完成
     *
     * @param statusMsg 提示信息
     */
    void onComplete(String statusMsg);

    /**
     * 重置
     */
    void onReset();
}

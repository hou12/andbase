[ ![Download](https://api.bintray.com/packages/cocolove2/maven/andbase/images/download.svg) ](https://bintray.com/cocolove2/maven/andbase/_latestVersion)


## 简介

library-andbase是一个快速开发的基础库，内置:`网络请求`,`MVP模式支持`,`常用工具类`,`常用的自定义view`等工具。

## 集成方式 
 
> compile 'com.cocolove2.library:andasse:{version}'




## demo示例

* 展示MVP的使用
* 网络请求相关功能`post get` `文件上传`,`文件下载`



## 使用文档

#### 一.基本类

* AndBaseActivity`所有activity的基类，内部提供通用的方法`

* AndBaseFragment`所有Fragment的基类`,`支持懒加载`

```
//注意: firstFragment 显示出来时布局还没有初始化完，所以第一个
//fragment的lazyLoad方法没有调用 可以在第一个fragment布局初始化成功后手动调用
//
public abstract void lazyLoad();
```

* ActivityContainer`activity栈的管理`


#### 二.MVP模式支持

关键类有`AndBaseMVPActivity`,`AndBaseMVPFragment`,`AndBasePresenter`和`IAndBaseMVPView`；
支持处理`activity`生命周期结束，请求未销毁造成的内存泄漏。详细使用参考Demo中[HttpTestActivity](https://gitee.com/cocolove2liu/andbase/blob/master/app/src/main/java/com/cocolove2/andbase/http/HttpTestActivity.java)






#### 三.网络请求 

>网络请求使用retrofit2+okhttp3+rxjava2主流开发二次封装

* **特性**

 1. 支持异步和同步网络请求`响应结果支持ResponseBody和实现IResponseResult接口的对象`
 2. 继承retrofit2+okhttp3+rxjava2所以功能
 3. 支持接口请求异常重试`主要用于token失效重刷token，再次请求`
 4. 支持异常统一处理
 5. 支持MVP模式
 6. 支持文件同步/异步下载(支持进度条)
 7. 支持文件同步/异步上传(支持进度条)
 8. 支持批量上传(支持进度条)
 9. 支持websocket 断开重连

 
 > 备注：
 1. 文件上传下载独立使用okhttp，不支持异常重试，异常统一处理；回调在子线程中
 2. 网络请求异常重试时，`retryWhenException`使用限制：
 ```
 1.响应结果实现{@link IResponseResult}接口时必回调该方法<br>
 
 2.响应结果为{@link ResponseBody}时，返回数据为json且包含{@link #getErrCodeKey()}关键字才会回调该方法
 ```   
 
*  **使用方式**
 
 1. 创建类`ApiHelper` 继承 `ABaseApi<ApiService>`，其中`ApiService`是接口协议
参考Demo中的类[ApiHelper](https://gitee.com/cocolove2liu/andbase/blob/master/app/src/main/java/com/cocolove2/andbase/ApiHelper.java)
 
 2. 基本网络请求，详细使用参考Demo中[HttpTestPresenter](https://gitee.com/cocolove2liu/andbase/blob/master/app/src/main/java/com/cocolove2/andbase/http/HttpTestPresenter.java)
 
```
//MVP模式
public class HttpTestPresenter extends BasePresenter<IHttpView> {

    public void getBaidu() {
        Disposable disposable = ApiHelper.getInstance().sendRequest(new ABaseApi.OnObservableListener<ResponseBody>() {
            @Override
            public Observable<ResponseBody> onObservable() {
                return ApiHelper.getInstance().getApiService().getBaidu();
            }
        }, new OnHttpListener<ResponseBody>() {
            @Override
            public void onSuccess(ResponseBody response) {
                if (isViewAttached()) {
                    try {
                        getView().onBaiduResult(response.string(), 0, "请求成功");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(int code, String msg) {
                if (isViewAttached()) {
                    getView().onBaiduResult("", code, msg);
                }
            }
        });

        addCompositeDisposable(disposable);
    }
}
```

3. websocket的使用 ,实例代码如下

```

  mWsManager = new WsManager.Builder().client(new OkHttpClient().newBuilder()
                .retryOnConnectionFailure(true)
                .pingInterval(10, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build())
                .needReconnect(true)
                .wsUrl(mWsUrl)
                .build();
        mWsManager.setWsStatusListener(wsStatusListener);
        mWsManager.startConnect();
        

    private WsStatusListener wsStatusListener = new WsStatusListener() {
        @Override
        public void onOpen(Response response) {
        }

        /**
         * 处理推送过来的消息
         * @param text 消息包
         */
        @Override
        public void onMessage(String text) {
        }
 /**
     * @param retryCount 重试次数
     * @return true 继续重连；false 停止重连，并重置次数
     */
        @Override
        public boolean onReconnect(int retryCount) {
            KLog.e(TAG, "========重连中==============>" + retryCount);
            return true;
        }

        @Override
        public void onClosing(int code, String reason) {
            super.onClosing(code, reason);
            KLog.e(TAG, "========关闭中==============>" + reason);
        }

        @Override
        public void onClosed(int code, String reason) {
            super.onClosed(code, reason);
            KLog.e(TAG, "========关闭==============>" + reason);
        }

        @Override
        public void onFailure(Throwable t, Response response) {
            super.onFailure(t, response);
        }
    };
    
    
    
     /**
         * 关闭websocket
         */
        private void closeSocket() {
            if (mWsManager != null) {
                mWsManager.stopConnect();
                mWsManager = null;
            }
        }

```



## 四.轻量级缓存管理

* `MulitipleCaches`缓存管理类,支持 内存缓存(bitmap/String)，sd卡缓存(最大100M)，SharedPreferences缓存

## 四.工具类

* 日志输出帮助类,支持输出json,xml,file日志`KLog`;
* 图片压缩处理，参考`ImageUtil`;
* 软键盘显示隐藏，以及高度的获取，参考`KeyBoardUtil`;
* 网络判断，以及网络信息获取,参考`NetworkUtil`;
* 加密解密`EncryptUtil`
* 文件信息处理帮助类`FileUtil`;
* 屏幕相关辅助类`ScreenUtils`
* 缓存数据清理`DataCleanManager`
* 状态栏字体颜色`StatusBarUtil` 
* 系统类型 `SystemUtil` 
* 常用正则表达式`RegexUtils`


